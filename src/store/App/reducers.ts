import { createReducer } from 'redux-act';
import defaultState from './state';
import { appInit, appInitSuccess, appInitFailure } from './actions';
import { AppStateType } from './type';

export function appInitReducer(state: AppStateType) {
  return {
    ...state,
    isLoaded: false,
    isLoading: true,
  };
}

export function appInitSuccessReducer(
  state: AppStateType,
  { config }: AppStateType
) {
  return {
    ...state,
    isLoading: false,
    isLoaded: true,
    config,
  };
}

export function appInitFailureReducer(state: AppStateType, error: Error) {
  return {
    ...state,
    isLoading: false,
    isLoaded: true,
    error,
  };
}

export default createReducer<AppStateType>(
  {
    [`${appInit}`]: appInitReducer,
    [`${appInitSuccess}`]: appInitSuccessReducer,
    [`${appInitFailure}`]: appInitFailureReducer,
  },
  defaultState
);
