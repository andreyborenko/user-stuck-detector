const translations = require('./translations');
const menu = require('./menu');
const subMenu = require('./subMenu');
const theme = require('./theme');

module.exports = {
  translations,
  ...menu,
  ...subMenu,
  ...theme,
};
