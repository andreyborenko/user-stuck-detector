import React from 'react';
import { mount } from 'enzyme';
import Layout from 'src/element/Layout';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<Layout {...props} />);

  return { props, wrapper };
};

describe('Layout', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
  it('should render Layout.Header without any crash', () => {
    const wrapper = mount(<Layout.Header />);
    expect(wrapper).toHaveLength(1);
  });
  it('should render Layout.Content without any crash', () => {
    const wrapper = mount(<Layout.Content />);
    expect(wrapper).toHaveLength(1);
  });
  it('should render Layout.Footer without any crash', () => {
    const wrapper = mount(<Layout.Footer />);
    expect(wrapper).toHaveLength(1);
  });
});
