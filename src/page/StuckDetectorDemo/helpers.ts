import { find, map, reduce } from 'lodash';
import { UserActionTypes } from './type';

export function objectEquals(x: any, y: any) {
  if (x === y) return true;
  // if both x and y are null or undefined and exactly the same
  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects
  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.
  for (var p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor
    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined
    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal
    if (typeof x[p] !== 'object') return false;
    // Numbers, Strings, export Functions, Booleans must be strictly equal
    if (!objectEquals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }
  for (p in y) if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
  // allows x[ p ] to be set to undefined
  return true;
}

export function findRepeats(array: any[], length: number) {
  const sub = array.slice(-length);
  for (let i = 1; i < array.length - length + 1; i++) {
    let compared = array.slice(-length - i, -i);
    if (objectEquals(sub, compared)) {
      return sub;
    }
  }
  return false;
}
export function checkCombinations(array: any[]) {
  for (let i = 6; i > 2; i--) {
    let repeats = findRepeats(array, i);
    if (repeats) {
      return repeats;
    }
  }
  return false;
}

export function handleEvent() {
  const eventLogJSON = localStorage.getItem('eventLog') || '[]';
  const events = JSON.parse(eventLogJSON);
  const slicedEvents = events.slice(-30);
  const repeatedCombination = checkCombinations(slicedEvents);
  if (!repeatedCombination) {
    return;
  }
  const incedentsJSON = localStorage.getItem('repeatedCombinations') || '[]';
  const incedents = JSON.parse(incedentsJSON);
  const uri =
    window.location.protocol +
    '//' +
    window.location.host +
    window.location.pathname +
    window.location.search;
  const incedent = {
    uri: uri,
    events: repeatedCombination,
  };
  incedents.push(incedent);
  window.localStorage.setItem(
    'repeatedCombinations',
    JSON.stringify(incedents)
  );
}

export function addToEventLog(item: any) {
  const eventLogJSON = localStorage.getItem('eventLog') || '[]';
  const eventLog = JSON.parse(eventLogJSON);
  eventLog.push(item);
  window.localStorage.setItem('eventLog', JSON.stringify(eventLog));
  handleEvent();
}

export function mapUsersToDropdownOptions(users: any) {
  return map(users, ({ id, name, ...rest }) => ({
    id,
    name,
    value: id,
    label: name,
    ...rest,
  }));
}

export const findUsers = (
  users1: any,
  users2: any,
  action: UserActionTypes
): any[] =>
  reduce(
    users1,
    (acc: any, initialUser) => {
      const foundUser = find(users2, { id: initialUser.id });
      return !foundUser ? [...acc, { ...initialUser, action }] : acc;
    },
    []
  );

export function getUsersFromLS() {
  return JSON.parse(localStorage.getItem('savedUsers') || '[]');
}

export function setUsersToLS(users: any) {
  return localStorage.setItem('savedUsers', JSON.stringify(users));
}

export function logActionsWithCriteria() {
  document.body.addEventListener('click', function (evt: any) {
    console.log(evt.target.innerText);
    console.log(getPathTo(evt.target));
    var pathTo = getPathTo(evt.target);
    var currentActionLog = localStorage.getItem('actionLog');
    var action = {
      url: window.location.href,
      pathTo,
      innerText: evt.target.innerText,
    };
    if (currentActionLog) {
      var currentActionLogArray = JSON.parse(currentActionLog);
      currentActionLogArray.push(action);
      localStorage.setItem('actionLog', JSON.stringify(currentActionLogArray));
    } else {
      localStorage.setItem('actionLog', JSON.stringify([action]));
    }
    var cr = checkWithCriteria(evt.target);
    if (cr) {
      var currentStats = localStorage.getItem('clickStats');
      // console.log(currentStats);
      if (currentStats) {
        var currentStatsObj = JSON.parse(currentStats);
        if (currentStatsObj[window.location.href]) {
          currentStatsObj[window.location.href].stats.counter++;
        } else {
          currentStatsObj[window.location.href] = {
            criteria: cr,
            stats: {
              innerText: evt.target.innerText,
              counter: 1,
            },
          };
        }
        var trackingInfo: any = currentStatsObj;
      } else {
        var trackingInfo: any = {
          [window.location.href]: {
            criteria: cr,
            stats: {
              innerText: evt.target.innerText,
              counter: 1,
            },
          },
        };
      }
      window.localStorage.setItem('clickStats', JSON.stringify(trackingInfo));
      console.log('logged! ' + JSON.stringify(trackingInfo));
      cr = '';
    }
  });
}

export function checkWithCriteria(element: any) {
  console.log('element', element);
  // var criteria = new Map([
  //   ['tag', 'SPAN'],
  //   ['title', 'CONFIRM'],
  // ]);
  var criteria2 = new Map([
    ['tag', 'SPAN'],
    ['title', 'SAVE'],
  ]);

  // var tag1 = criteria.get('tag');
  // var title1 = criteria.get('title');
  // if (tag1 && title1) {
  //   if (element.tagName == tag1 && element.innerText == title1) {
  //     return 'confirm button';
  //   }
  // }

  var tag = criteria2.get('tag');
  var title = criteria2.get('title');
  console.log('tag', tag);
  console.log('title', title);
  if (tag && title) {
    if (element.tagName == tag && element.innerText == title) {
      return 'save button';
    }
  }
  return false;
}

export function getPathTo(element: any): any {
  if (element.id !== '') {
    return 'id("' + element.id + '")';
  }
  if (element === document.body) {
    return element.tagName;
  }
  var ix = 0;
  var siblings = element.parentNode.childNodes;
  for (var I = 0; I < siblings.length; I++) {
    var sibling = siblings[I];
    if (sibling === element) {
      return (
        getPathTo(element.parentNode) +
        '/' +
        element.tagName +
        '[' +
        (ix + 1) +
        ']'
      );
    }
    if (sibling.nodeType === 1 && sibling.tagName === element.tagName) {
      ix++;
    }
  }
}
