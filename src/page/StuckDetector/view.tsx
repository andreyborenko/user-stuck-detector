import React from 'react';
import { Card, Heading } from 'elmo-elements';
import { HeaderV2, Layout } from 'src/element';

import './style.scss';

export const StuckDetectorView = () => {
  const repeatedCombinationsJSON = localStorage.getItem('repeatedCombinations');
  const repeatedCombinations: any = repeatedCombinationsJSON
    ? JSON.parse(repeatedCombinationsJSON)
    : null;
  const clickStatsJSON = localStorage.getItem('clickStats');
  const clickStats: any = clickStatsJSON ? JSON.parse(clickStatsJSON) : null;
  let clickStatsKey = '';

  if (clickStats) {
    for (const [key, value] of Object.entries(clickStats)) {
      clickStatsKey = key;
    }
  }

  return (
    <>
      <Layout.Header>
        <HeaderV2
          className={'header-v2-layout'}
          title={'Stuck Detector Dashboard'}
        />
      </Layout.Header>

      <Layout.Content>
        <Heading className="mb-3">Detected Repeated Combinations</Heading>

        <div className="dashboard-grid">
          <div>
            {repeatedCombinations
              ?.reverse()
              .map((repeatedCombination: any, i: number) => (
                <Card key={i}>
                  <div className="dashboard-item">
                    <p>URL: {repeatedCombination.uri}</p>
                    <div>
                      Often repeated combinations of actions:
                      {repeatedCombination.events.map(
                        (event: any, iE: number) => {
                          return (
                            <div key={iE}>
                              - {event.label} - action type: {event.type}
                            </div>
                          );
                        }
                      )}
                    </div>
                  </div>
                </Card>
              )) ?? <Card>No data yet</Card>}
          </div>
          {clickStats && clickStats[clickStatsKey] ? (
            <Card className="dashboard-item" heading="Click Stats">
              <p>URL: {clickStatsKey}</p>
              <p>Criteria: {clickStats[clickStatsKey].criteria}</p>
              <p>Clicks counter: {clickStats[clickStatsKey].stats.counter}</p>
            </Card>
          ) : (
            <Card className="dashboard-item" heading="Click Stats">
              No data yet
            </Card>
          )}
        </div>
      </Layout.Content>
    </>
  );
};

export default StuckDetectorView;
