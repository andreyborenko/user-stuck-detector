/**
 * This is a local version of a Mock API
 */
const path = require('path');
const jsonServer = require('json-server');
const routes = require('./routes');
const middlewares = require('./middlewares');
const config = require('./config');
const rewriter = require('./rewriter');
const serveStatic = require('serve-static');

const server = jsonServer.create();
const router = jsonServer.router(routes);

server.use(serveStatic(path.resolve(__dirname, 'assets')));
server.use(jsonServer.rewriter(rewriter));
server.use(middlewares);

server.use(jsonServer.bodyParser);
// We need to refactor this, this is adding course object to response of all post request
server.use((req, res, next) => {
  // Continue to JSON Server router
  next();
});

server.use(router);
server.listen(config.port, () => {
  console.log(`JSON Server is running on port: ${config.port}`);
});
