import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Notification } from 'elmo-elements';
import FlashMessagesView from 'src/lib/flashMessages/view';

jest.mock('react-redux', () => {
  const originalModule = jest.requireActual('react-redux');
  const dispatch = jest.fn();

  return {
    ...originalModule,
    Provider: ({ children }: any) => children,
    useDispatch: jest.fn(() => dispatch),
    dispatch,
  };
});

const mountComponent = (customProps: any = {}) => {
  const defaultProps = {
    message: {},
    ...customProps,
  };
  const wrapper = mount(<FlashMessagesView {...defaultProps} />);

  return { props: defaultProps, wrapper };
};

describe('FlashMessagesView', () => {
  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
    });

    it('should be match with snapshot', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toMatchSnapshot();
    });

    it('should not render Notification', () => {
      const { wrapper } = mountComponent({
        message: null,
      });
      expect(wrapper.find(Notification)).toHaveLength(0);
    });
  });

  describe('interaction', () => {
    it('should trigger `handleClose` prop', () => {
      const mockCallback = jest.fn();
      const message = { message: 'test message' };
      const { wrapper } = mountComponent({
        handleClose: mockCallback,
        message,
      });

      const onClose = wrapper.find(Notification).prop('onClose');

      act(() => {
        onClose();
      });

      expect(mockCallback).toHaveBeenCalledWith(message);
    });

    it.each(['onUndo', 'handleClose'])(
      'should trigger `%s` prop',
      (funcName) => {
        const mockCallbacks = {
          onUndo: jest.fn(),
          handleClose: jest.fn(),
        };
        const message = { onUndo: mockCallbacks.onUndo };
        const { wrapper } = mountComponent({
          handleClose: mockCallbacks.handleClose,
          message,
        });

        const onUndo = wrapper.find(Notification).prop('onUndo');

        act(() => {
          onUndo();
        });

        expect(mockCallbacks[funcName]).toHaveBeenCalled();
      }
    );
  });
});
