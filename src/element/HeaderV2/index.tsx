import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import { HeaderLayout, HeaderAside, HeaderTitle, HeaderWrapper } from './style';

interface Props {
  title?: string;
  className?: string;
  tabs?: React.ReactNode;
  children?: React.ReactNode;
  aside?: React.ReactNode;
}

export const HeaderV2 = (props: Props) => {
  const { title, tabs, children, aside, className } = props;
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);

  function resizeEventHandler() {
    setScreenWidth(window.innerWidth);
  }

  useEffect(() => {
    window.addEventListener('resize', resizeEventHandler);

    return () => window.removeEventListener('resize', resizeEventHandler);
  }, [screenWidth]);

  const isDesktop = screenWidth > 840;

  return (
    <HeaderLayout className={className}>
      <HeaderWrapper className={classnames('header-v2', { 'with-tabs': tabs })}>
        {title && (
          <HeaderTitle className="header-v2__title">{title}</HeaderTitle>
        )}

        {isDesktop && children}
        <div>{tabs}</div>

        {aside && <HeaderAside>{aside}</HeaderAside>}
      </HeaderWrapper>
      {!isDesktop && children}
    </HeaderLayout>
  );
};

HeaderV2.defaultProps = {
  title: undefined,
  tabs: undefined,
  className: undefined,
  children: undefined,
  aside: undefined,
};

export default HeaderV2;

export { default as HeaderTabs } from './component/HeaderTabs';
