import React from 'react';
import { mount } from 'enzyme';
import { App, mapDispatchToProps, mapStateToProps } from 'src/App';
import { AppLoading } from 'src/layout';
import { getTheme } from 'src/lib/theme';
import { appInit } from 'src/store/App';

jest.mock('src/lib/theme', () => ({
  getTheme: () => {
    return { data: 'fakeTheme' };
  },
}));

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<App {...props} />);

  return { props, wrapper };
};

describe('App', () => {
  const mockAppInit = jest.fn();

  it('should run mockAppInit on componentDidMount', () => {
    const fakeProps = { appInit: mockAppInit, app: 1 };
    const fakeIsLoaded = 'isLoaded';
    const fakeError = 'some error';
    const fakeTheme = 'some theme';
    const AppLoadingProps = { isLoaded: fakeIsLoaded, error: fakeError };
    const { wrapper } = mountComponent({
      ...fakeProps,
      app: { ...AppLoadingProps },
    });

    expect(wrapper).toMatchSnapshot();

    mockAppInit.mockClear();
    const instace = wrapper.instance();
    instace.componentDidMount();
    expect(mockAppInit).toBeCalledTimes(1);

    // AppLoading and props
    expect(wrapper.find(AppLoading).prop('isLoaded')).toBe(fakeIsLoaded);
    expect(wrapper.find(AppLoading).prop('error')).toBe(fakeError);
  });

  it('theme data from getTheme should be setState and passed to ThemeProvider', async () => {
    const fakeProps = { appInit: mockAppInit, app: 1 };
    const fakeIsLoaded = 'isLoaded';
    const fakeError = 'some error';
    const fakeTheme = 'some theme';
    const AppLoadingProps = { isLoaded: fakeIsLoaded, error: fakeError };
    const { wrapper } = mountComponent({
      ...fakeProps,
      app: { ...AppLoadingProps },
    });
    const renderedTheme = await getTheme();
    expect(wrapper.state().theme).toStrictEqual(renderedTheme);
  });

  it('mapStateToProps', () => {
    const fakeData = { app: 'some app' };
    const result = mapStateToProps(fakeData);
    expect(result).toStrictEqual(fakeData);
  });

  it('mapDispatchToProps', () => {
    const result = mapDispatchToProps;
    expect(result).toStrictEqual({ appInit: appInit });
  });
});
