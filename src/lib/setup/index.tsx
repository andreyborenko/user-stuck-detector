import { loadTranslations } from 'src/lib/translation';
// import { authenticatingUser } from '../auth';
// import { loadAssets } from '../assets';
import { loadServerConfig } from 'src/lib/config/loadder';
import { ErrorAppInitUndefinedError } from 'src/lib/error';

/**
 * App setup and initialisations
 *
 * <Dev.Note>
 *  Any async/sync activities that need to be done first in order to load the app can be impelemnted here.
 *  As you can see we dont implement the logic related to each action in this file so we keep this setup as
 *  minimal and abstract as posible. In otherword the job of this `setup` function is to orchestrate some
 *  individual set up activities.
 * </Dev.Note>
 */
function setup(): Promise<any> {
  const promises: Promise<any>[] = [
    loadServerConfig(),
    // authenticatingUser(), it's commented out until we need it.
    loadTranslations(),
    // loadAssets(), it's commented out until we need it.
  ];

  return Promise.all(promises)
    .then(([config]: any) => ({ config }))
    .catch((error: any) => {
      // <Dev.Note>
      // The following condition should happen very rarely. It means you have an exception but there
      // is no valid error for that. For example you return a Promise.reject() isnide a function.
      // </Dev.Note>
      if (!error) {
        error = new ErrorAppInitUndefinedError();
      }
      return Promise.reject(error);
    });
}

export default setup;
