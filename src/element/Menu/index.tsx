import React, { Component } from 'react';
import { withRouter, matchPath } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu as ElmoMenu, SearchIcon } from 'elmo-elements';
import NavigationService from './service';
import { MENU_KEY, MENU_ADMIN_KEY, SUBMENU } from 'src/lib/const';
import { ReduxStoreType } from 'src/store/type';
import { InterfaceLink } from './model';
import { getNavigationSuccessAction } from 'src/modules/actions';
import { map } from 'lodash';

class Menu extends Component<any, any> {
  public state = NavigationService.getInitState();

  static defaultProps = {
    appFlags: {},
  };

  public async componentDidMount() {
    const { storeNavigation } = this.props;

    try {
      const mainMenu = await NavigationService.getData();
      const learningSubMenu = await NavigationService.getLearningSubMenuData();
      const navigation = { ...mainMenu, ...learningSubMenu };

      storeNavigation(navigation);

      this.setState({
        navigation,
        loaded: true,
      });
    } catch (error) {
      this.setState({ loaded: true });
    }
  }

  public componentDidUpdate(prevProps: any, prevState: any) {
    const { loaded } = this.state;

    if (prevState.loaded !== loaded) {
      this.makeNavigation();
    }
  }

  getSubMenuItems() {
    const {
      navigation: {
        subMenu: { modules },
      },
    } = this.state;

    return modules.map((item, key) => {
      const returnValue = {
        title: item.title,
        url: item.url,
        handleClick: () => {
          this.props.history.push(item.url);
        },
        icon: item.icon,
        isSelected: this.isSelected(item),
      };

      return {
        ...returnValue,
        // ...overrideLearningSubMenuByFlag(this.props.appFlags, item),
      };
    });
  }

  getAdminInfo() {
    const {
      navigation: {
        menu: { user },
      },
    } = this.state;
    return user.filter((n) => n.key === MENU_ADMIN_KEY).shift();
  }

  getUserInfo() {
    const {
      navigation: {
        user,
        status: { location },
      },
    } = this.state;

    const { name, img } = user;

    // handle admin information
    const adminInfo = this.getAdminInfo();
    let isAdmin = false;
    let adminUrl = '';
    let isAdminSelected = false;

    if (adminInfo && 'url' in adminInfo) {
      isAdmin = true;
      adminUrl = adminInfo.url;
      isAdminSelected = (location && location.key) === adminInfo.key;
    }

    return {
      name,
      avatar: img,
      isAdmin,
      adminUrl,
      isAdminSelected,
    };
  }

  getUserMenu() {
    const {
      navigation: {
        menu: { user },
      },
    } = this.state;

    // remove admin item from the menu, we are presenting the admin info in the user details object
    const userMenu = user.filter((n) => n.key !== MENU_ADMIN_KEY);

    return userMenu.map((item) => ({
      title: item.title,
      url: item.url,
      icon: item.icon,
    }));
  }

  getMainItems() {
    const {
      navigation: {
        menu: { modules },
      },
    } = this.state;

    return modules.map((item) => ({
      title: item.title,
      url: item.url,
      icon: item.icon === 'elmo-icon-search' ? SearchIcon : item.icon,
      isSelected: item.key === MENU_KEY,
    }));
  }

  getCurrentHost() {
    window.location.href = `//${window.location.host}`;
  }

  matchPathNotNull = (path: string): boolean =>
    matchPath(window.location.pathname, {
      path,
    }) !== null;

  isSelected = (item: InterfaceLink): boolean => {
    if (this.matchPathNotNull(item.url)) {
      return true;
    }

    if (item.key !== SUBMENU.dashboard) {
      return false;
    }

    return false;
    // return (
    // this.matchPathNotNull(defaultRoutes.viewCourseEnrolment.path)
    // );
  };

  makeNavigation() {
    const { navigation } = this.state;
    const modules = map(navigation.menu.modules, (item) => {
      return item;
    });

    this.setState((state: any) => ({
      ...state,
      navigation: {
        ...state.navigation,
        menu: {
          ...state.navigation.menu,
          modules: [...modules],
        },
      },
    }));
  }

  public render() {
    const {
      navigation: { branding },
      loaded,
    } = this.state;
    return (
      <ElmoMenu
        logo={branding.logo}
        asSubMenu={true}
        subMenuItems={this.getSubMenuItems()}
        menuItems={this.getMainItems()}
        userDetails={this.getUserInfo()}
        userMenu={this.getUserMenu()}
        theme={branding.theme}
        isReact={true}
        isLoading={!loaded}
        logoHref={this.getCurrentHost}
        {...(branding.smallLogo !== ''
          ? { mobileLogo: branding.smallLogo }
          : {})}
      />
    );
  }
}

export default Menu;

const mapStateToProps = (state: ReduxStoreType) => ({
  appFlags: state.app.config.flags,
});

const mapDispatchToProps = {
  storeNavigation: getNavigationSuccessAction,
};

export const MenuWithRouter = connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Menu));
