import React from 'react';
import { Select } from 'elmo-elements';
import { SelectProps } from 'elmo-elements/Select';
import { debounce } from 'lodash';

export type SelectWithPaginationQueryType = {
  q: string;
  page: number;
};

export type SelectWithPaginationProps = {
  isLoading?: boolean;
  limitPerPage?: number;
  totalOptions: number;
  isLastPage?: boolean;
  onQueryChange: (query: SelectWithPaginationQueryType) => void;
};

function SelectWithPagination({
  limitPerPage = 10,
  isLoading,
  totalOptions,
  isLastPage,
  ...props
}: SelectProps & SelectWithPaginationProps) {
  const [query, updateQuery] = React.useState<SelectWithPaginationQueryType>({
    q: '',
    page: 1,
  });

  const changeQuery = (newQuery: SelectWithPaginationQueryType) => {
    updateQuery(newQuery);
    props.onQueryChange(newQuery);
  };

  const onSearch = (keyword: string) => {
    if (keyword !== query.q || query.page !== 1) {
      changeQuery({
        ...query,
        q: keyword,
        page: 1,
      });
    }
  };

  const onScrollToBottom = () => {
    if (!isLastPage) {
      changeQuery({
        ...query,
        page: query.page + 1,
      });
    }
  };

  return (
    <Select
      {...props}
      options={props.options}
      className={`${props.className} ${isLoading ? 'is-searching' : ''}`}
      onMenuScrollToBottom={debounce(onScrollToBottom, 100)}
      onInputChange={debounce(onSearch, 300)}
    />
  );
}

SelectWithPagination.defaultProps = {
  isLoading: false,
  limitPerPage: 10,
  onQueryChange: () => {},
  className: '',
};

export default SelectWithPagination;
