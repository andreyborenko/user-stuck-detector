import React from 'react';
import { Radio, RadioGroup } from 'elmo-elements';
import { map } from 'lodash';
import { HeaderTabsType } from 'src/modules/types';
import './style.scss';

export type PropsType = {
  items: HeaderTabsType[];
  handleChange: (value: string | number) => void;
  selected: string;
};

const HeaderTabs = ({ items, handleChange, selected }: PropsType) => (
  <RadioGroup
    className="header-v2-tabs"
    selected={selected}
    onChange={handleChange}
  >
    {map(items, (item) => (
      <Radio key={item.label} value={item.value}>
        {item.label}
      </Radio>
    ))}
  </RadioGroup>
);

export default HeaderTabs;
