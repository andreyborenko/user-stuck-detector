import * as LDClient from 'launchdarkly-js-client-sdk';
import logger from 'src/lib/logger';
import { getConfig } from 'src/lib/config';
import { CLIENT } from 'src/lib/const';

const { launchDarklyKey } = getConfig();

export function loadServerConfig(): any {
  // LaunchDarkly
  const ldclient = LDClient.initialize(launchDarklyKey, {
    key: CLIENT,
  });
  return ldclient
    .waitForInitialization()
    .then(() => ({
      flags: ldclient.allFlags(),
    }))
    .catch((err) => {
      logger.error('error on config');
      return { flags: {} };
    });
}
