/**
 * <Dev.Note>
 *  Any parameter that can be different from one environment to another can be stored here.
 *  In this file we overide the default config for the development env
 * </Dev.Note>
 */
import config from './config.production';

config.api.baseUrl = `http://localhost:${process.env.MOCK_SERVER_PORT || 3010}${
  config.api.baseUrl
}`;
config.api.menuUrl = `http://localhost:${process.env.MOCK_SERVER_PORT || 3010}${
  config.api.menuUrl
}`;
config.api.fileUrl = `http://localhost:${process.env.MOCK_SERVER_PORT || 3010}${
  config.api.fileUrl
}`;
config.api.preferenceUrl = `http://localhost:${
  process.env.MOCK_SERVER_PORT || 3010
}${config.api.preferenceUrl}`;
config.assetUrl = '';

config.launchDarklyKey = '5e60a3db0cd67409b0773218';

export default config;
