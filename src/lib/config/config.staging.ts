/**
 * <Dev.Note>
 *  Any parameter that can be different from one environment to another can be stored here.
 *  In this file we overide the default config for the staging env
 * </Dev.Note>
 */
import config from './config.production';

config.launchDarklyKey = '5e60a3c80cd67409b0773212';

export default config;
