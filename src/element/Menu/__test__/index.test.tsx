import React from 'react';
import { mount } from 'enzyme';
import { default as LearningMenu } from 'src/element/Menu';
import {
  CourseCatalogueIcon,
  CourseRequestIcon,
  ExternalTrainingIcon,
  Menu,
} from 'elmo-elements';
// TODO: Export from routes
import MyLearningRoutes from 'src/page/MyLearning/routes';
import CourseCatalogueRoutes from 'src/page/CourseCatalogue/routes';
import { ViewCourseEnrolmentRoutes } from 'src/page/ViewCourse/routes';
import {
  ViewActivityFaceToFaceRoutes,
  ViewActivityPageRoutes,
} from 'src/page/ViewActivity/routes';
import config from 'src/lib/config';
import { SUBMENU_FLAGS } from 'src/lib/const';
const actions = require('src/modules/actions');
jest.mock('src/modules/actions');
import { storeCurrentUserAction } from 'src/modules/actions/user';

config.api.menuUrl = '/';

jest.mock('src/lib/logger', () => false);
jest.mock('elmo-elements');

const mockLearningIcon = <></>;
const mockCourseCatalogueIcon = CourseCatalogueIcon;
const mockExternalTrainingIcon = ExternalTrainingIcon;

const mockMenuApi = {
  user: {
    id: 2,
    name: 'ELMO Admin Generic',
    img:
      'https://learningapi.dev.elmodev.com/media/cache/resolve/thumb_40_40/files/learningapi/ab/58/ab58b5bc646550a7dde2303cbfa8bb11ad66d4cd.jpg',
  },
  branding: {
    logo:
      'https://learningapi.dev.elmodev.com/imagecache/learningapi/origin/files/learningapi/85/af/85af1bf94f94d305b27ba67f4b0e371553082ad5.png',
    smallLogo: null,
    logoLink: 'https://learningapi.dev.elmodev.com',
    companyName: 'learningapi',
    version: '',
    theme: {
      profile: { color: '#428bca', backgroundColor: '#ffffff' },
      admin: { color: '#000000', backgroundColor: '#ffffff' },
      menu: {
        backgroundColor: '#195799',
        fontColor: '#ffffff',
        hoverFontColor: '#ffffff',
        font: 'Arial, \u0022Helvetica Neue\u0022, Helvetica, sans-serif',
      },
    },
  },
  menu: {
    user: [
      {
        key: 'app.menu.changePassword',
        icon: 'ChangePasswordIcon',
        title: 'Change Password',
        description: '',
        url:
          'https://learningapi.dev.elmodev.com/controlpanel/change-password/',
        children: [],
      },
      {
        key: 'app.menu.signOut',
        icon: 'SignOutIcon',
        title: 'Sign Out',
        description: '',
        url: '/logout',
        children: [],
      },
      {
        key: 'app.menu.admin',
        icon: 'elmo-icon-admin',
        title: 'Administration',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/admin/',
        children: [],
      },
    ],
    modules: [
      {
        key: 'app.menu.home',
        icon: 'elmo-icon-home',
        title: 'Home',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/dashboard',
        children: [],
      },
      {
        key: 'app.menu.myProfile',
        icon: 'elmo-icon-my-profile',
        title: 'Profile',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/controlpanel/my-profile',
        children: [],
      },
      {
        key: 'app.menu.myEmployees',
        icon: 'elmo-icon-my-employees',
        title: 'My Team',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/controlpanel/team-members',
        children: [],
      },
      {
        key: 'app.menu.learning',
        icon: 'elmo-icon-learning',
        title: 'Learning',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/learning/my-learning',
        children: [],
      },
      {
        key: 'app.menu.performance',
        icon: 'elmo-icon-performance',
        title: 'Performance',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/dashboard/my-performance',
        children: [],
      },
      {
        key: 'app.menu.survey',
        icon: 'elmo-icon-survey',
        title: 'Survey',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/admin/survey/management',
        children: [],
      },
      {
        key: 'app.menu.reports',
        icon: 'elmo-icon-reports',
        title: 'Reports',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/report',
        children: [],
      },
    ],
  },
  status: { location: { key: null, title: null } },
};

const mockSubmenuAPI = {
  subMenu: {
    modules: [
      {
        key: 'app.learning.submenu.myLearning',
        title: 'My Learning',
        url: '/learning/my-learning',
        description: '',
        icon: mockLearningIcon,
      },
      {
        key: 'app.learning.submenu.myExternalTraining',
        title: 'External Training',
        url: '/learning/my-external-training',
        description: '',
        icon: mockExternalTrainingIcon,
      },
      {
        key: 'app.learning.submenu.courseCatalogue',
        title: 'Course Catalogue',
        url: '/learning/courses',
        description: '',
        icon: mockCourseCatalogueIcon,
      },
      {
        key: 'app.learning.submenu.request',
        title: 'Course Requests',
        url: '/learning/course-requests',
        icon: CourseRequestIcon,
        description: '',
        external: true,
      },
    ],
  },
};

jest.mock('src/element/Menu/service', () => ({
  getInitState: jest.fn(() => {
    return {
      error: undefined,
      navigation: {
        branding: {
          logo: 'initLogo',
          smallLogo: 'initSmallLogo',
          logoLink: '/',
          companyName: 'learningapi',
          version: '',
          theme: {},
        },
        user: {
          id: '',
          name: '',
          img: '-',
        },
        menu: {
          user: [],
          home: null,
          modules: [],
        },
        subMenu: {
          modules: [],
        },
        status: {
          location: {
            key: '',
            title: '',
          },
        },
      },
      loaded: false,
    };
  }),
  getData: () => {
    return mockMenuApi;
  },

  getLearningSubMenuData: () => {
    return mockSubmenuAPI;
  },
}));

const storeCurrentUser = jest.fn();

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps, storeCurrentUser };
  const wrapper = mount(<LearningMenu {...props} />);
  const instance = wrapper.instance();
  const component = wrapper.find('.elmo-nav-menu');

  return { props, wrapper, component, instance };
};

describe('Menu', () => {
  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
    });

    it('should be match with snapshot', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toMatchSnapshot();
    });

    it('should pass NavigationService.getInitState to props', () => {
      jest.clearAllMocks();
      mountComponent();
      expect(Menu).toBeCalledTimes(1);
      expect(Menu).toBeCalledWith(
        expect.objectContaining({
          asSubMenu: true,
          isLoading: true,
          isReact: true,
          logo: 'initLogo',
          logoHref: expect.any(Function),
          menuItems: [],
          subMenuItems: [],
          mobileLogo: 'initSmallLogo',
          theme: {},
          userDetails: {
            adminUrl: '',
            avatar: '-',
            isAdmin: false,
            isAdminSelected: false,
            name: '',
          },
          userMenu: [],
        }),
        {}
      );
    });

    describe('highlight current submenu', () => {
      const { location } = window;
      beforeAll(() => {
        delete window.location;
        jest.clearAllMocks();
      });
      afterAll(() => {
        window.location = location;
      });
      it.each([
        [MyLearningRoutes.path, MyLearningRoutes.title.default],
        [ViewCourseEnrolmentRoutes.path, MyLearningRoutes.title.default],
        [CourseCatalogueRoutes.path, CourseCatalogueRoutes.title.default],
        [
          ViewActivityFaceToFaceRoutes.path,
          ViewActivityFaceToFaceRoutes.title.default,
        ],
        [ViewActivityPageRoutes.path, ''],
      ])(
        'should set isSelected true if the path( %s ) matches %s',
        (customPathname, submenuTitle) => {
          window.location = {
            pathname: customPathname,
          };
          const { instance } = mountComponent(<LearningMenu />);
          instance.state.navigation.subMenu = mockSubmenuAPI.subMenu;
          const resultGetSubMenuItems = instance.getSubMenuItems();
          resultGetSubMenuItems.map((item) => {
            if (item.title === submenuTitle) {
              expect(item.isSelected).toBe(true);
            }
          });
        }
      );

      // this test should be updated along with releasing new pages
      it('handleClick should be undefined if external is true', () => {
        const { instance } = mountComponent(<LearningMenu />);
        instance.state.navigation.subMenu = mockSubmenuAPI.subMenu;
        const resultGetSubMenuItems = instance.getSubMenuItems();
        resultGetSubMenuItems.map((item) => {
          if (item.url === '/learning/course-requests') {
            expect(item.handleClick).toBe(undefined);
          }
        });
      });
    });

    describe('Launch Darkly', () => {
      it('should update submenu item url if LD is true', () => {
        const { wrapper } = mountComponent({
          appFlags: {
            [SUBMENU_FLAGS.courseCatalogue.flagKey]: true,
          },
        });
        wrapper.instance().state.navigation.subMenu = mockSubmenuAPI.subMenu;
        const resultGetSubMenuItems = wrapper.instance().getSubMenuItems();
        resultGetSubMenuItems.map((item) => {
          if (item.title === 'Course Catalogue') {
            expect(
              item.url === SUBMENU_FLAGS.courseCatalogue.v1Url
            ).toBeFalsy();
          }
        });
      });

      it('should not update submenu item url if LD is false or undefined', () => {
        const { wrapper } = mountComponent();
        wrapper.instance().state.navigation.subMenu = mockSubmenuAPI.subMenu;
        const resultGetSubMenuItems = wrapper.instance().getSubMenuItems();
        resultGetSubMenuItems.map((item) => {
          if (item.title === 'Course Catalogue') {
            expect(
              item.url === SUBMENU_FLAGS.courseCatalogue.v1Url
            ).toBeTruthy();
          }
        });
      });
    });
  });

  describe('interaction', () => {
    it('should redirect to root(`/`) on getCurrentHost', () => {
      jest.clearAllMocks();
      global.window.host = 'http://localhost/';

      const { instance } = mountComponent();
      instance.getCurrentHost();
      expect(window.location.href).toBe(global.window.host);
    });
  });
});
