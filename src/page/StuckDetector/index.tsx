import React, { lazy } from 'react';
import Suspense from 'src/element/Suspense';

export const StuckDetectorView = lazy(() => import('./view'));

const StuckDetector = () => (
  <Suspense>
    <StuckDetectorView />
  </Suspense>
);

export default StuckDetector;
