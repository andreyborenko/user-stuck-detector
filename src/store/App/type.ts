export type AppConfigType = {
  flags: {
    [key: string]: boolean | string;
  };
};

export type AppStateType = {
  isLoaded: boolean;
  isLoading: boolean;
  config: AppConfigType;
  error: Error | null;
};
