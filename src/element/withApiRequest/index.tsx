import React, { useEffect, useState } from 'react';
import { Api } from 'src/modules/utils/api';
import { invoke } from 'lodash';

type Props = {
  request: string;
  params: any;
};

export const withApiRequest = (Component: any, { request, params }: Props) => (
  props: any
) => {
  const [isLoaded, setLoadedState] = useState(false);
  const [isLoading, setLoadingState] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    try {
      setLoadingState(true);
      invoke(Api, request, params).then((res: any) => {
        setLoadingState(false);

        if (res.data) {
          setLoadedState(true);
          setData(res.data);
        }
      });
    } catch (e) {
      setLoadedState(true);
      setError(e);
    }
  }, []);

  return (
    <Component {...props} response={{ isLoaded, isLoading, data, error }} />
  );
};

export default withApiRequest;
