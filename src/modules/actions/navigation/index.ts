import { createAction } from 'redux-act';
import { NavigationType } from 'src/modules/types';

export const getNavigationAction = createAction<any>('GET_NAVIGATION');

export const getNavigationSuccessAction = createAction<NavigationType>(
  'GET_NAVIGATION_SUCCESS'
);

export const getNavigationFailureAction = createAction<Error>(
  'GET_NAVIGATION_FAILURE'
);
