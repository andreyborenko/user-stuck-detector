/**
 * This is a Lambda verison of mock API and should be used only for staging/production
 */
const serverless = require('serverless-http');
const jsonServer = require('json-server');
const logger = require('pino')(
  {
    prettyPrint: true,
  },
  process.stderr
);

const routes = require('./routes');
const middlewares = require('./middlewares');
const config = require('./config');
const rewriter = require('./rewriter');

const server = jsonServer.create();
const router = jsonServer.router(routes);
const request = async () => {
  try {
    server.use(jsonServer.rewriter(rewriter));
    server.use(middlewares);
    server.use(router);
  } catch (e) {
    if (e.code === 'ExpiredToken') {
      logger.error(`Please add valid credentials for AWS. Error: ${e.message}`);
    } else {
      logger.error(e.code);
    }
  }
};

const handler = serverless(server);
module.exports.handler = async (event, context) => {
  await request();
  event.path = event.path.substring('/api'.length);
  const result = await handler(event, context);
  return result;
};

function start() {
  // start the web server
  const { port } = config;
  return server.listen(port, () => {
    logger.info(
      `JSON Server is running under port ${port}. Use http://localhost:${port} to access it`
    );
  });
}

if (require.main === module) {
  request();
  start();
}
