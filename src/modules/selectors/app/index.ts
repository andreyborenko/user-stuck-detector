import { get } from 'lodash';
import { createSelector } from 'reselect';
import { AppConfigType, AppStateType } from 'src/store/App/type';
import { ReduxStoreType } from 'src/store/type';

export const appSelector = (state: ReduxStoreType): AppStateType =>
  get(state, 'app');

export const appConfigSelector = createSelector(
  appSelector,
  (app: AppStateType) => get(app, 'config')
);

export const appConfigFlagsSelector = createSelector(
  appConfigSelector,
  (config: AppConfigType) => get(config, 'flags')
);
