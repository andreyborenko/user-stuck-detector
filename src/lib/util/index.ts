import { forEach } from 'lodash';
import classNames from 'classnames';
import logger from 'src/lib/logger';
import { SCREEN_WIDTH_MOBILE } from '../const';

/**
 * Extract the component name
 *
 * @param Component
 */
export function getComponentName(Component: any) {
  return Component.displayName || Component.name || 'Component';
}

/**
 *
 * Make a BEM string based on props
 * @param  {string} name Base class.
 * @param  {Object<string, any>} classes Component classes.
 * @param  {Object<string, any>} modifiers Component extra class modifiers passing as props.
 * @return {string} BEM class string.
 */
export function getClass(
  name: string,
  classes?: string,
  modifiers?: Record<string, any>
): string {
  const modifierClasses: any = {};

  if (modifiers) {
    forEach(modifiers, (_, key) => {
      if (key === 'className') {
        modifierClasses[`${modifiers[key]}`] = true;
      } else if (modifiers[key]) {
        modifierClasses[`${name}--${key}`] = modifiers[key];
      }
    });
  }

  return classNames(name, classes, modifierClasses);
}

/**
 * Get data from Localstorage
 */
export function getLocalstorage(
  key: string,
  format: 'string' | 'object' = 'string'
): any {
  try {
    const localStorageData = localStorage.getItem(key);
    if (localStorageData) {
      if (format === 'string') {
        return localStorageData;
      }
      if (format === 'object') {
        try {
          const localStorageDataObject = JSON.parse(localStorageData);
          return localStorageDataObject;
        } catch (e) {
          return false;
        }
      }
    }
    return false;
  } catch (error) {
    logger.error(error);
  }

  return false;
}

/**
 * Set data to Localstorage
 */
export function setLocalstorage(
  key: string,
  data: any,
  format: 'string' | 'object' = 'string'
) {
  try {
    if (localStorage) {
      if (format === 'string') {
        localStorage.setItem(key, data);
      }
      if (format === 'object') {
        localStorage.setItem(key, JSON.stringify(data));
      }
    }
  } catch (error) {
    logger.error(error);
  }
}

/**
 * Get data from sessionStorage
 */
export function getSessionstorage(
  key: string,
  format: 'string' | 'object' = 'string'
): any {
  try {
    const sessionStorageData = sessionStorage.getItem(key);
    if (sessionStorageData) {
      if (format === 'string') {
        return sessionStorageData;
      }
      if (format === 'object') {
        try {
          const sessionStorageDataObject = JSON.parse(sessionStorageData);
          return sessionStorageDataObject;
        } catch (e) {
          return false;
        }
      }
    }
    return false;
  } catch (error) {
    logger.error(error);
  }

  return false;
}

/**
 * Set data to sessionStorage
 */
export function setSessionstorage(
  key: string,
  data: any,
  format: 'string' | 'object' = 'string'
) {
  try {
    if (sessionStorage) {
      if (format === 'string') {
        sessionStorage.setItem(key, data);
      }
      if (format === 'object') {
        sessionStorage.setItem(key, JSON.stringify(data));
      }
    }
  } catch (error) {
    logger.error(error);
  }
}

/**
 * A document title(Head > Title) helper
 */
export function setPageTitle(title: string) {
  document.title = title;
}

export function isMobileMode() {
  return window.innerWidth < SCREEN_WIDTH_MOBILE;
}
