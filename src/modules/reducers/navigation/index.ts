import { createReducer } from 'redux-act';
import initialState from './state';
import {
  getNavigationAction,
  getNavigationSuccessAction,
  getNavigationFailureAction,
} from 'src/modules/actions';
import { NavigationStateType, NavigationType } from 'src/modules/types';

export const onNavigationRequested = (state: NavigationStateType) => ({
  ...state,
  ...initialState,
  isLoading: true,
});

export const onNavigationSuccess = (
  state: NavigationStateType,
  payload: NavigationType
) => ({
  ...state,
  data: payload,
  isLoading: false,
  isLoaded: true,
});

export const onNavigationFailure = (
  state: NavigationStateType,
  payload: Error
) => ({
  ...state,
  ...initialState,
  error: payload,
});

export const navigationReducer = createReducer<NavigationStateType>(
  {},
  initialState
)
  .on(getNavigationAction, onNavigationRequested)
  .on(getNavigationSuccessAction, onNavigationSuccess)
  .on(getNavigationFailureAction, onNavigationFailure);

export default navigationReducer;
