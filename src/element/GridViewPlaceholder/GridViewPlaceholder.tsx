import React, { ReactNode } from 'react';
import { Card, Row, Col, Loader } from 'elmo-elements';
import { GridViewPlaceholderPropsType } from './type';
import './GridViewPlaceholder.scss';

/**
 * Card image
 */

const image = <Loader type="card-image" width="100%" />;

/**
 * Get cols
 *
 * @param cols
 */
function getCols(cols: number, colResponsiveOptions: any): ReactNode[] {
  const elements: ReactNode[] = [];
  const size = 24 / cols;
  for (let i = 0; i < cols; i += 1) {
    // TODO add card-content type when elmo-elements part would be deployed
    const type = 'card-content';

    elements.push(
      <Col
        key={`grid-view-placeholder-col-${i}`}
        md={size}
        sm={size}
        lg={size}
        {...colResponsiveOptions}
        role="presentation"
      >
        <Card
          // TODO remove heading and add fullWidthHeader when elmo-elements part would be deployed
          fullWidthHeader={image}
        >
          <Loader type={type} />
        </Card>
      </Col>
    );
  }

  return elements;
}

/**
 * Get rows and cols
 *
 * @param rows
 * @param cols
 * @param colResponsiveOptions
 * @param types
 */
function getRows(
  rows: number,
  cols: number,
  colResponsiveOptions: any
): ReactNode[] {
  const elements: ReactNode[] = [];
  for (let i = 0; i < rows; i += 1) {
    elements.push(
      <Row
        isNoGutters={false}
        key={`grid-view-placeholder-row-${i}`}
        role="presentation"
      >
        {getCols(cols, colResponsiveOptions)}
      </Row>
    );
  }

  return elements;
}

function GridViewPlaceholder({
  id,
  cols,
  rows,
  ...colResponsiveOptions
}: GridViewPlaceholderPropsType) {
  return (
    <div data-testid="grid-view-placeholder" className="grid-view-placeholder">
      {getRows(rows as number, cols as number, colResponsiveOptions)}
    </div>
  );
}

GridViewPlaceholder.defaultProps = {
  cols: 12,
  rows: 1,
};

export default GridViewPlaceholder;
