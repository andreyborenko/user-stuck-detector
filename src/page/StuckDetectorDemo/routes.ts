import StuckDetector from '.';
import { setBasePath } from 'src/lib/route';

const StuckDetectorDemoRoutes: any = {
  path: setBasePath('/demo'),
  component: StuckDetector,
  title: 'Stuck Detector Demo',
};

export default StuckDetectorDemoRoutes;
