import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeFlashMessage, dismissFlashMessage } from './actions';
import FlashMessagesView from './view';
import { Message } from './types';
import { ReduxStoreType } from 'src/store/type';

const MessageRemoveTimer = 1000;
const MessageDismissTimer = 500;

export interface FlashMessagesProps {
  messages: Message[];
  dismissFlashMessage: (message: Message) => void;
  removeFlashMessage: (message: Message) => void;
}

export class FlashMessagesContainer extends Component<any, FlashMessagesProps> {
  handleClose = (message: Message) => {
    // eslint-disable-next-line no-shadow
    const { removeFlashMessage, dismissFlashMessage } = this.props;
    dismissFlashMessage({ id: message.id });
    setTimeout(
      () => removeFlashMessage({ id: message.id }),
      MessageDismissTimer
    );
  };

  render() {
    const { messages } = this.props;
    if (messages && messages.length > 0) {
      const message = [...messages].shift();
      if (message.isActive) {
        setTimeout(() => this.handleClose(message), MessageRemoveTimer);
      }
      return (
        <FlashMessagesView message={message} handleClose={this.handleClose} />
      );
    }
    return null;
  }
}

const mapStateToProps = (state: ReduxStoreType) => ({
  messages: state.flashMessages.messages,
});

const mapDispatchToProps = {
  dismissFlashMessage,
  removeFlashMessage,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlashMessagesContainer);
