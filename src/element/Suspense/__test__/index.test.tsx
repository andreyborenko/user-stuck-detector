import React from 'react';
import { mount } from 'enzyme';
import Suspense from 'src/element/Suspense';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<Suspense {...props} />);

  return { props, wrapper };
};

describe('Suspense', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
});
