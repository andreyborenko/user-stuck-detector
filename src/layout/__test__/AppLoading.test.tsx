import React from 'react';
import { mount } from 'enzyme';
import AppLoading from 'src/layout/AppLoading';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<AppLoading {...props} />);

  return { props, wrapper };
};

describe('AppLoading', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
  it('should render component accordingly if error is passed', () => {
    const { wrapper } = mountComponent({ error: true });
    expect(wrapper).toHaveLength(1);
  });
  it('should render children if isLoaded is passed', () => {
    const Children = <div className="test-children">dummy children</div>;
    const { wrapper } = mountComponent({ isLoaded: true, children: Children });

    expect(wrapper).toHaveLength(1);
    expect(wrapper.find('.test-children')).toHaveLength(1);
  });
});
