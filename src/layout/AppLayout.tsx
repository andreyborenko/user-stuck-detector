import React from 'react';
import { NestedRoutes } from 'src/lib/route';
import { appRoutes } from 'src/routes';
import './App.scss';

/**
 *
 * For Suspense please see: https://reactjs.org/docs/code-splitting.html
 * Also plase see src/lib/lazy/LazyLoading.tsx If you dont use withLazyLoading
 * then we will fullback on the AppLayout Suspense component.
 */
function AppLayout() {
  return <NestedRoutes routes={appRoutes} />;
}

export default AppLayout;
