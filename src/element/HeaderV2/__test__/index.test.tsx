import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { Select } from 'elmo-elements';
import HeaderV2 from 'src/element/HeaderV2';
import { HeaderTitle } from 'src/element/HeaderV2/style';

const mountComponent = (
  customProps: { title: string; children: React.ReactNode } = {
    title: 'Test title',
    children: <Select />,
  }
) => {
  const props = { ...customProps };
  const wrapper = mount(<HeaderV2 {...props} />);

  return { props, wrapper };
};

describe('HeaderV2', () => {
  let wrapper: ReactWrapper;

  beforeEach(() => {
    wrapper = mountComponent().wrapper;
  });

  it('should render component without any crash', () => {
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });

  it('should render component with children and title', () => {
    expect(wrapper.find(Select)).toHaveLength(1);
    expect(wrapper.find(HeaderTitle)).toHaveLength(1);
    expect(wrapper.find(HeaderTitle).text()).toBe('Test title');
  });
});
