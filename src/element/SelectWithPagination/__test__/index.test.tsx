import React from 'react';
import { mount } from 'enzyme';
import _ from 'lodash';
import { Select } from 'elmo-elements';
import { act } from 'react-dom/test-utils';
import SelectWithPagination from 'src/element/SelectWithPagination';

_.debounce = jest.fn((fn) => fn);

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<SelectWithPagination {...props} />);

  return { props, wrapper };
};

describe('SelectWithPagination', () => {
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState');
  useStateSpy.mockImplementation((init) => [init, setState]);

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
    });

    it('should be match with snapshot', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('props', () => {
    describe('custom', () => {
      it('should pass `is-searching` className in Select component', () => {
        const { wrapper } = mountComponent({ isLoading: true });
        expect(
          wrapper.find(Select).prop('className')?.includes('is-searching')
        ).toBe(true);
      });

      it('should not pass `is-searching` className in Select component', () => {
        const { wrapper } = mountComponent({ isLoading: false });
        expect(
          wrapper.find(Select).prop('className')?.includes('is-searching')
        ).toBe(false);
      });
    });
  });

  describe('interaction', () => {
    describe('scroll to bottom', () => {
      it('should change `page`', () => {
        const { wrapper } = mountComponent({
          isLastPage: false,
        });
        const onMenuScrollToBottom = wrapper
          .find(Select)
          .prop('onMenuScrollToBottom');

        act(() => {
          onMenuScrollToBottom();
        });

        expect(setState).toHaveBeenCalledWith({
          page: 2,
          q: '',
        });
      });

      it('should not change `page`', () => {
        const { wrapper } = mountComponent({
          isLastPage: true,
        });
        const onMenuScrollToBottom = wrapper
          .find(Select)
          .prop('onMenuScrollToBottom');

        act(() => {
          onMenuScrollToBottom();
        });

        expect(setState).not.toHaveBeenCalledWith({
          page: 2,
          q: '',
        });
      });
    });

    describe('search', () => {
      it('should update search query', () => {
        const searchQuery = 'test search';
        const { wrapper } = mountComponent();
        const onSearch = wrapper.find(Select).prop('onInputChange');

        act(() => {
          onSearch(searchQuery);
        });

        expect(setState).toHaveBeenCalledWith({
          page: 1,
          q: searchQuery,
        });
      });

      it('should not update search query', () => {
        const searchQuery = '';
        const { wrapper } = mountComponent();
        const onSearch = wrapper.find(Select).prop('onInputChange');

        act(() => {
          onSearch(searchQuery);
        });

        expect(setState).not.toHaveBeenCalledWith({
          page: 1,
          q: searchQuery,
        });
      });
    });
  });
});
