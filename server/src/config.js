module.exports = {
  total: process.env.TOTAL_NUMBER_OF_DATA || 100,
  port: process.env.MOCK_SERVER_PORT || 3000,
  learningApiBaseUrl: process.env.LEARNING_API_BASE_URL || '/v1/learning-api',
  fileApiBaseUrl: process.env.FILE_API_BASE_URL || '/api/v0',
};
