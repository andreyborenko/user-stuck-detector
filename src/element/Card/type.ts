import { ButtonProps } from 'elmo-elements/Button';
import { ReactNode } from 'react';

type BadgeProps = {
  /**
   * Badge type, use one from the following:
   * BadgeType = 'grey' | 'outlined' | 'warning' | 'info' | 'success' | 'danger'
   */
  type: 'grey' | 'outlined' | 'warning' | 'info' | 'success' | 'danger';
  /**
   * Badge role, use one from the following:
   * BadgeRole = 'info' | 'status' | 'label'
   */
  role?: 'info' | 'status' | 'label';
  /** Custom id attribute */
  id?: string;
  /** Add custom class to class attribute */
  className?: string;
  /** To enable popover Icon */
  hasPopoverIcon?: boolean;
  /** Badge icon */
  icon?: ReactNode;
  /** The content of a badge */
  children?: ReactNode;
  /** To render badge in circular */
  isCircular?: boolean;
};

export interface CardProps {
  /** Card id attribute, optional id */
  id?: string;
  /** Card heading title */
  heading?: ReactNode;
  /** Card header button */
  headerButtons?: ButtonProps[];
  /** Card description */
  description?: string;
  /** Card content */
  children: ReactNode;
  /** Additional class */
  className?: string;
  /** Make height 100% */
  isFullHeight?: boolean;
  /** To place option content to the right side of heading. such as Button */
  option?: ReactNode;
  /** small heading above heading */
  prefix?: string;
  /** image in the header */
  image?: string;
  /** image alt/title */
  imageAlt?: string;
  /** array of the badge components which are displayed on image */
  badges?: BadgeProps[];
  /** function that is triggered if the card is clicked */
  onClickCard?: () => void;
  /** Role is for WCAG accessibility compliance (optional) */
  role?: string;
}
