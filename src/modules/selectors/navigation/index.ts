import { get } from 'lodash';
import { createSelector } from 'reselect';
import { ReduxStoreType } from 'src/store/type';
import {
  NavigationStateType,
  NavigationType,
  SubMenuType,
} from 'src/modules/types';

export const getNavigationSelector = (
  state: ReduxStoreType
): NavigationStateType => state.navigation;

export const getNavigationDataSelector = createSelector(
  getNavigationSelector,
  (data: NavigationStateType): NavigationType | null => data.data
);

export const getSubMenuItemsSelector = createSelector(
  getNavigationDataSelector,
  (navigation: NavigationType | null): SubMenuType[] =>
    get(navigation, 'subMenu.modules', [])
);

export const getAppFlagsSelector = (state: ReduxStoreType): any =>
  get(state, 'app.config.flags');

export const getCurrentUserNameSelector = createSelector(
  getNavigationDataSelector,
  (navigation): string => get(navigation, 'user.name', '')
);
