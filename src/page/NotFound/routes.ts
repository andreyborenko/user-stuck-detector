import NotFound from '.';
import { setBasePath } from 'src/lib/route';

type NotFoundRoutesType = {
  path: string;
  component: React.FC;
};

const NotFoundRoutes: NotFoundRoutesType = {
  path: setBasePath('/not-found'),
  component: NotFound,
};

export default NotFoundRoutes;
