import { HttpClient } from 'elmo-react-core';
import { SearchIcon, RemoveRedEyeIcon } from 'elmo-elements';
import httpClient from 'src/lib/api';
import config from 'src/lib/config';
import { t, messages } from 'src/lib/translation';
import { getEnv } from 'src/lib/env';
import { InterfaceNavigation, Navigation, InterfaceAppState } from './model';
import StuckDetectorDashboardRoutes from 'src/page/StuckDetector/routes';
import StuckDetectorDemoRoutes from 'src/page/StuckDetectorDemo/routes';
import { SUBMENU as STUCK_DETECTOR_SUBMENU } from 'src/lib/const';

// Need to check where the view Course pages are belong to
const learningRoutes = {
  StuckDetectorRoutes: StuckDetectorDashboardRoutes,
  StuckDetectorDemoRoutes: StuckDetectorDemoRoutes,
};

export const StuckDetectorSubMenuItems = {
  dashboard: {
    key: STUCK_DETECTOR_SUBMENU.dashboard,
    title: 'Dashboard',
    url: learningRoutes.StuckDetectorRoutes.path,
    icon: SearchIcon,
    description: '',
  },
  demo: {
    key: STUCK_DETECTOR_SUBMENU.demo,
    title: 'Demo',
    url: learningRoutes.StuckDetectorDemoRoutes.path,
    icon: RemoveRedEyeIcon,
    description: '',
  },
};

export const menuHttpClient = new HttpClient({
  baseURL: config.api.menuUrl,
  useMockData: getEnv() === 'development',
});

/**
 * NavigationService implements the core logics of Navigation app
 */
class NavigationService {
  /**
   * getInitState returns the initial state of the application
   */
  public getInitState(): InterfaceAppState {
    return {
      error: undefined,
      navigation: new Navigation(),
      loaded: false,
    };
  }

  public async getData(): Promise<InterfaceNavigation> {
    try {
      const serverResponse: any = await menuHttpClient
        .get('', {})
        .then((result: any) => result);

      if (!serverResponse || !serverResponse.data) {
        return Promise.reject(t(messages.app.menu.receivedInvalidData));
      }

      const response = serverResponse.data;
      return Object.assign(new Navigation(), response);
    } catch (err) {
      return Promise.reject(t(messages.app.menu.receivedInvalidData));
    }
  }

  public async getLearningSubMenuData(): Promise<InterfaceNavigation> {
    try {
      const serverResponse = await httpClient
        .get('links')
        .then((result: any) => result);
      if (!serverResponse || !serverResponse.data) {
        return Promise.reject(t(messages.app.menu.receivedInvalidData));
      }

      const response = serverResponse.data;
      return Object.assign(this.mapResourcetoMenuItems(response));
    } catch (err) {
      return Promise.reject(t(messages.app.menu.receivedInvalidData));
    }
  }

  private mapResourcetoMenuItems(learningSubMenuResources: any) {
    const submenuItems: any = [];

    learningSubMenuResources.forEach((element: any) => {
      switch (element.rel) {
        case 'Dashboard':
          submenuItems.push(StuckDetectorSubMenuItems.dashboard);
          break;
        case 'Demo':
          submenuItems.push(StuckDetectorSubMenuItems.demo);
          break;
        default:
      }
    });

    return { subMenu: { modules: submenuItems } };
  }
}

export default new NavigationService();
