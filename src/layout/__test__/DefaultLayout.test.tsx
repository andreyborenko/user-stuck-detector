import React from 'react';
import { shallow } from 'enzyme';
import DefaultLayout from 'src/layout/DefaultLayout';
import { NestedRoutes } from 'src/lib/route';

const shallowComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = shallow(<DefaultLayout {...props} />);

  return { props, wrapper };
};

describe('DefaultLayout', () => {
  it('should render component without any crash', () => {
    const { wrapper } = shallowComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
  it('should render component with routes succesfully', () => {
    const customProps = { routes: 'some value' };
    const { wrapper } = shallowComponent(customProps);
    expect(wrapper).toHaveLength(1);
    const wrapperNestedRoutes = wrapper.find(NestedRoutes);
    expect(wrapperNestedRoutes.length).toBe(1);
    expect(wrapperNestedRoutes.props()).toMatchObject(customProps);
  });
});
