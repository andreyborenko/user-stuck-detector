import React from 'react';
import { Button, Select, Heading } from 'elmo-elements';
import { mapUsersToDropdownOptions } from '../../helpers';

type Props = any;

export const Users = ({
  users,
  selectedUsers,
  onSelectUsers,
  onSave,
  onCancel,
}: Props) => (
  <div>
    <Heading className="">Select the users</Heading>
    <Select
      isMulti={true}
      options={mapUsersToDropdownOptions(users)}
      value={selectedUsers}
      className="users-select mt-3"
      onChange={(...args: any) => {
        const [newOptions, { action, removedValue, option }] = args;
        const modifiedOption = removedValue || option;
        onSelectUsers(action, modifiedOption, newOptions);
      }}
      hideSelectedOptions={true}
    />

    <div className="mt-4 d-flex flex-justify-content-center">
      <Button className="mr-2" onClick={onSave} type="primary">
        Save
      </Button>
      <Button onClick={onCancel} type="primary">
        Cancel
      </Button>
    </div>
  </div>
);
