import { ReactNode } from 'react';

export type MessageType = 'warning' | 'info' | 'success' | 'danger';
export const MessageTypes: { [key: string]: MessageType } = {
  warning: 'warning',
  info: 'info',
  success: 'success',
  danger: 'danger',
};

export interface Message {
  /** Show | Hide notification */
  isActive?: boolean;
  /** Notification type */
  type?: MessageType;
  /** Notification content */
  message?: string;
  /** On close callback */
  onClose?: any | undefined;
  /** On Undo callback, if added, the undo button will appear */
  onUndo?: any | undefined;
  /** icon */
  icon?: ReactNode;
  /** on mouser over callback */
  onmouserover?: () => void;
  /** on mouser out callback */
  onmouseout?: () => void;
  /** id attribute */
  id?: string;
  undoBtnText: string;
  className?: string;
  closeModal?: boolean;
}

export interface FlashMessages {
  messages?: any;
}
