import * as styledComponents from 'styled-components';
import { defaultTheme } from 'elmo-elements';
import { HttpClient } from 'elmo-react-core';
import config from 'src/lib/config';
import { getEnv } from 'src/lib/env';
import { THEME_KEY, THEME_EXPIRY_TIME } from 'src/lib/const';
import { getSessionstorage, setSessionstorage } from 'src/lib/util';

export const themeHttpClient = new HttpClient({
  baseURL: `${config.api.preferenceUrl}/theme`,
  useMockData: getEnv() === 'development',
});

export function mapTheme(themeData: any) {
  return {
    font: themeData.siteFont && themeData.siteFont,
    link: {
      color: themeData.linkColor && themeData.linkColor,
    },
    button: {
      color: themeData.buttonFontColor && themeData.buttonFontColor,
      background: themeData.buttonColor && themeData.buttonColor,
      hover: themeData.buttonHoverColor && themeData.buttonHoverColor,
      active: themeData.buttonHoverColor && themeData.buttonHoverColor,
    },
    menu: {
      fontColor: themeData.menuFontColor && themeData.menuFontColor,
      backgroundColor: themeData.menuColor && themeData.menuColor,
      hoverFontColor:
        themeData.menuHoverFontColor && themeData.menuHoverFontColor,
      headingBannerUserMenuColor:
        themeData.headingBannerUserMenuColor &&
        themeData.headingBannerUserMenuColor,
      headingBannerBgColor:
        themeData.headingBannerBgColor && themeData.headingBannerBgColor,
    },
  };
}

export async function fetchTheme() {
  try {
    const serverResponse: any = await themeHttpClient
      .get('', {})
      .then((result: any) => result);

    if (!serverResponse || !serverResponse.data) {
      return Promise.reject(new Error('error'));
    }

    const response = serverResponse.data;

    return mapTheme(response);
  } catch (err) {
    return Promise.reject(new Error('error'));
  }
}

export async function loadTheme(): Promise<any> {
  const storedTheme = getSessionstorage(THEME_KEY, 'object');
  const currentTime = Date.now();
  let theme = null;
  try {
    if (
      storedTheme &&
      storedTheme.timestamp &&
      currentTime - storedTheme.timestamp < THEME_EXPIRY_TIME
    ) {
      theme = storedTheme.data;
    }
  } catch (error) {
    return Promise.reject(error);
  }

  if (!theme) {
    const fetchedTheme = await fetchTheme();
    theme = fetchedTheme;
    const sessionstorageThemeData = {
      timestamp: currentTime,
      data: fetchedTheme,
    };
    setSessionstorage(THEME_KEY, sessionstorageThemeData, 'object');
  }

  return Promise.resolve();
}

export async function getTheme() {
  await loadTheme();
  const storedTheme = getSessionstorage(THEME_KEY, 'object');
  const theme = {
    ...defaultTheme,
    ...storedTheme.data,
  };
  return theme;
}

const {
  ThemeProvider,
  withTheme,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
  typeof defaultTheme
>;

export { ThemeProvider, withTheme };
