import * as actions from 'src/lib/flashMessages/actions';

jest.mock('shortid', () => ({
  generate: jest.fn(() => 1),
}));

describe('actions', () => {
  const defaultPayload = { someKeyword: 'some value' };

  describe('addFlashMessage', () => {
    it('should return generated payload id', () => {
      const payload = {};
      const expectedResult = {
        error: false,
        payload: { id: 1 },
        type: 'PMS_ADD_FLASH_MESSAGE',
      };

      const result = actions.addFlashMessage(payload);
      expect(result).toMatchObject(expectedResult);
    });

    it('should return generated payload id', () => {
      const payload = { id: 2 };
      const expectedResult = {
        error: false,
        payload: payload,
        type: 'PMS_ADD_FLASH_MESSAGE',
      };

      const result = actions.addFlashMessage(payload);
      expect(result).toMatchObject(expectedResult);
    });
  });

  it.each([
    ['showFlashMessage', defaultPayload, 'PMS_SHOW_FLASH_MESSAGE'],
    ['removeFlashMessage', defaultPayload, 'PMS_REMOVE_FLASH_MESSAGE'],
    ['dismissFlashMessage', defaultPayload, 'PMS_DISMISS_FLASH_MESSAGE'],
  ])('%s should return as expected', (functionKey, payload, actionType) => {
    const expectedResult = {
      error: false,
      payload: payload,
      type: actionType,
    };

    const result = actions[functionKey](payload);
    expect(result).toMatchObject(expectedResult);
  });
});
