export type SubMenuType = {
  description?: string;
  icon: any;
  key: string;
  title: string;
  url: string;
  external?: boolean;
};

export type SubMenuModulesType = {
  modules: SubMenuType[];
};

export type BrandingType = {
  logo: string;
  smallLogo: string;
  logoLink: string;
  companyName: string;
  version: string;
  theme: any;
};

export type CurrentUserType = {
  id: number;
  img: string;
  isElmo: boolean;
  name: string;
};

export type MainMenuType = {
  user: {
    key: string;
    icon: string;
    title: string;
    description: string;
    url: string;
    children: [];
  }[];
  modules: {
    key: string;
    icon: string;
    title: string;
    description: string;
    url: string;
    children: [];
  }[];
};

export type NavigationType = {
  user: CurrentUserType;
  branding: BrandingType;
  menu: MainMenuType;
  subMenu: SubMenuModulesType;
  status: {
    location: {
      key: string | null;
      title: string | null;
    };
  };
};

export type NavigationStateType = {
  isLoading: boolean;
  isLoaded: boolean;
  data: NavigationType | null;
  error: Error | null;
};
