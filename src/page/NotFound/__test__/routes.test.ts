import NotFoundRoutes from 'src/page/NotFound/routes';

jest.mock('src/lib/route', () => ({
  setBasePath: jest.fn((v) => {
    return v;
  }),
}));

describe('NotFoundRoutes', () => {
  it('should render component without any crash', () => {
    expect(NotFoundRoutes.path).toBe('/not-found');
  });
});
