import React from 'react';
import { getComponentName } from 'src/lib/util';
import Suspense from 'src/element/Suspense';

/**
 * A HOC to display a lazy loaded component. It will use a PageLoading as a
 * fallback when the component is not still loaded.
 */
function withLazyLoading(Component: any, Fallback?: any) {
  const LazyLoading = (props: any) => {
    const { routes } = props;
    return (
      <Suspense fallback={Fallback}>
        <Component routes={routes} />
      </Suspense>
    );
  };

  LazyLoading.displayName = `LazyLoading(${getComponentName(Component)})`;

  return LazyLoading;
}

export default withLazyLoading;
