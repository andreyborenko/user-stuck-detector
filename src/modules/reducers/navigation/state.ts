import { NavigationStateType } from 'src/modules/types';

/**
 * Document about what this state is about
 */
export default {
  isLoading: false,
  isLoaded: false,
  data: null,
  error: null,
} as NavigationStateType;
