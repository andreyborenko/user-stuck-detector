const CommonTranslations = {
  app: {
    notFound: {
      title: {
        key: 'app.notFound.title',
        default: 'Not Found - default',
      },
      content: {
        key: 'app.notFound.content',
        default:
          'The server returned a "404 Not Found". Try returning to your home page and try again. ',
      },
    },
    dashboard: {
      key: 'app.menu.dashboard',
      default: 'Dashboard Default',
    },
    language: {
      key: 'app.menu.language',
      default: 'Language Default',
    },
    menu: {
      receivedInvalidData: {
        key: 'app.menu.receivedInvalidData',
        default: 'Received data is not valid',
      },
    },
    dropAreaContent: {
      title: {
        key: 'app.dropAreaContent.title',
        default: 'Choose a file or drag it here',
      },
      message: {
        key: 'app.dropAreaContent.message',
        default: 'Individual file size limit {{fileMaxSize}} MB',
      },
    },
    uploadFile: {
      warning: {
        title: {
          key: 'app.uploadFile.warning.title',
          default: 'Error',
        },
        message: {
          key: 'app.uploadFile.warning.message',
          default: `You're trying to upload invalid file type or the file size is exceeding limit.`,
        },
        okLabel: {
          key: 'app.uploadFile.warning.okLabel',
          default: 'Ok',
        },
      },
      sizeLimitInfo: {
        key: 'app.uploadFile.sizeLimitInfo',
        default: 'Individual file size limit:',
      },
      supportedFormatsInfo: {
        key: 'app.uploadFile.supportedFormatsInfo',
        default: 'Supported file formats:',
      },
    },
  },

  position: {
    word: {
      courses: {
        type: {
          recommended: {
            key: 'position.word.courses.type.recommended',
            default: 'Recommended',
          },
          required: {
            key: 'position.word.courses.type.required',
            default: 'Required',
          },
        },
      },
    },
  },
  professionalDevelopment: {
    title: {
      key: 'professionalDevelopment.title',
      default: 'Professional Development',
    },
  },
  pages: {
    myLearning: {
      key: 'pages.myLearning',
      default: 'My Learning',
    },
    courseCatalogue: {
      key: 'pages.courseCatalogue',
      default: 'Course Catalogue',
    },
    manageExternalTraining: {
      key: 'pages.manageExternalTraining',
      default: 'Add External training',
    },
    editExternalTraining: {
      key: 'pages.editExternalTraining',
      default: 'Edit External training',
    },
    viewCourse: {
      key: 'pages.viewCourse',
      default: 'View Course',
    },
    viewCourseEnrolment: {
      key: 'pages.viewCourseEnrolment',
      default: 'View Course Enrolment',
    },
    viewFaceToFaceActivity: {
      key: 'pages.viewFaceToFaceActivity',
      default: 'FaceToFace Activity Sessions List',
    },
    viewPageActivity: {
      key: 'pages.viewCourse',
      default: 'Page Activity',
    },
    viewQuizActivity: {
      key: 'pages.viewQuizActivity',
      default: 'Quiz Activity',
    },
    viewAcknowledgementActivity: {
      key: 'pages.viewAcknowledgementActivity',
      default: 'Acknowledgement Activity',
    },
    viewMyExternalTraining: {
      key: 'pages.viewMyExternalTraining',
      default: 'View My External Training',
    },
    myExternalTraining: {
      key: 'pages.myExternalTraining',
      default: 'My External Training',
    },
    viewActivity: {
      key: 'pages.viewActivity',
      default: 'FaceToFace Activity Sessions List',
    },
  },
};

export const appTranslations = {
  ...CommonTranslations,
};
