import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRouteComponent(props: any) {
  const isAuthorized = true;
  if (isAuthorized) {
    return <Route {...props} />;
  }

  return <Redirect to="/login" />;
}

const PrivateRoute = PrivateRouteComponent;

export default PrivateRoute;
