import { Route } from 'react-router-dom';
import { setBasePath } from './lib/route';

import NotFound from './page/NotFound';

// App layouts
import DefaultLayout from './layout/DefaultLayout';
import NotFoundRoutes from './page/NotFound/routes';
import StuckDetectorDashboardRoutes from './page/StuckDetector/routes';
import StuckDetectorDemoRoutes from './page/StuckDetectorDemo/routes';

export const pageRoutes = {
  stuckDetector: StuckDetectorDashboardRoutes,
  stuckDetectorDemo: StuckDetectorDemoRoutes,
};
export const defaultRoutes = {
  rootRedirect: {
    path: '/',
    redirect: StuckDetectorDashboardRoutes.path,
  },
  baseRedirect: {
    path: setBasePath(''),
    redirect: StuckDetectorDashboardRoutes.path,
  },
  ...pageRoutes,
  notFound: {
    path: NotFoundRoutes.path,
    component: NotFound,
    routeComponent: Route,
  },

  redirect: {
    path: '*',
    redirect: NotFoundRoutes.path,
  },
};

// AppRoutes defines the routings with app layout
export const appRoutes = {
  default: {
    path: '/',
    component: DefaultLayout,
    routeComponent: Route,
    routes: defaultRoutes,
  },
};

export default defaultRoutes;
