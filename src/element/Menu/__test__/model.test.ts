import {
  Status,
  Theme,
  Branding,
  User,
  Link,
  Menu,
  Navigation,
  SubMenu,
} from 'src/element/Menu/model';

describe('Menu Model', () => {
  describe('rendering', () => {
    it('should construct Status', () => {
      const status = new Status();
      expect(status).toEqual({ location: { key: '', title: '' } });
    });
    it('should construct Theme', () => {
      const theme = new Theme();
      expect(theme).toEqual({
        container: {
          padding: '1em',
          width: '256px',
        },
        profile: {
          color: '#1b4da2',
          backgroundColor: '#fff',
        },
        admin: {
          color: '#1b4da2',
          backgroundColor: '#fff',
        },
        menu: {
          padding: '1em',
          font: 'Verdana',
          fontColor: '#fff',
          hoverFontColor: '#fff',
          backgroundColor: '#1b4da2',
          backgroundColorHover: '#123f8d',
        },
      });
    });
    it('should construct Branding', () => {
      const branding = new Branding();
      expect(branding).toEqual({
        logo: '',
        smallLogo: '',
        logoLink: '',
        companyName: '',
        version: '',
        theme: new Theme(),
      });
    });
    it('should construct User', () => {
      const user = new User();
      expect(user).toEqual({
        id: '',
        name: '',
        img: '',
      });
    });
    it('should construct Link', () => {
      const link = new Link();
      expect(link).toEqual({
        description: '',
        icon: '',
        key: '',
        title: '',
        url: '',
      });
    });
    it('should construct Menu', () => {
      const menu = new Menu();
      expect(menu).toEqual({
        user: [new Link()],
        home: new Link(),
        modules: [new Link()],
      });
    });
    it('should construct Navigation', () => {
      const navigation = new Navigation();
      expect(navigation).toEqual({
        branding: new Branding(),
        user: new User(),
        menu: new Menu(),
        status: new Status(),
        subMenu: new SubMenu(),
      });
    });
  });
});
