import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { PropsType } from 'src/element/HeaderV2/component/HeaderTabs';
import HeaderTabs from 'src/element/HeaderV2/component/HeaderTabs';

const mockHistoryPush = jest.fn((value) => value);
jest.mock('react-router-dom', () => {
  const originalModule = jest.requireActual('react-router-dom');
  return {
    ...originalModule,
    useHistory: jest.fn(() => ({
      push: mockHistoryPush,
    })),
  };
});

const mockedProps: PropsType = {
  items: [
    { value: '-1', label: 'All' },
    { value: '2', label: 'Recommended' },
  ],
  handleChange: jest.fn(),
  selected: '-1',
};

describe('HeaderTabs', () => {
  let wrapper: ReactWrapper;

  beforeAll(() => {
    wrapper = mount(<HeaderTabs {...mockedProps} />);
  });

  it('Should mount properly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('Should change filter on click', async () => {
    const RecommendedBtn = wrapper.find('.elmo-radio').last();
    RecommendedBtn.simulate('click', { force: true });
    expect(RecommendedBtn.hasClass('.elmo-radio--checked'));
  });
});
