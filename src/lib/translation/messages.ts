import { appTranslations } from 'src/translations';

export const LANGUAGES = {
  en: 'en',
};

export const messages = appTranslations;
