// Implement your oauth utility functions here
export function isAuthorized() {
  return Promise.resolve(true);
}

// Some extra things we might need to do before we can load the app
// For example authenticating a user
export function authenticatingUser(): Promise<any> {
  return Promise.resolve({
    id: 123,
    userName: 'John Doe',
    isAdmin: false,
    avatar: '',
  });
}
