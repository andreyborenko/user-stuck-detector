import { HttpClient } from 'elmo-react-core';
import config from 'src/lib/config';
import logger from 'src/lib/logger';
import { getEnv } from 'src/lib/env';
import { Dialog } from 'elmo-elements';

// <Dev.Note.Security>
//  The principle we follow here is that in order to make your app more secure
//  try to think before you are using a 3rd party libraray and validate if you really need it.
//  If the answer is yes it is better to wrap your 3rd party apis. For example we have a
//  wrapper around axios that provides you a HttpClient
//
//  You dont need to wrap the entire react :) or redux but it make sence for use to wrap a Logger
//  libraray or a HttpClient libraray.
// </Dev.Note.Security>

// Check original request url and respnose url.
export const checkRedirection = (response: any) => {
  const parser = document.createElement('a');

  // The below to avoid unnecessary redirection as IE doesn't support responseURL
  if (response.request.responseURL === undefined) {
    return response;
  }
  parser.href = response.request.responseURL;
  const responseURL = parser.pathname;
  parser.href = response.config.url;
  const originURL = parser.pathname;
  if (originURL === responseURL) {
    return response;
  }
  // The below is commented temporarily until the issue(LMS2-363) fixes on TMS level
  // window.location.href = response.request.responseURL;

  // The below is a temporary solution for the issue(LMS2-363)
  window.location.href = '/learning/my';
  return false;
};

// Setup a custom HttpClient
const httpClient = new HttpClient({
  baseURL: config.api.baseUrl,
  useMockData: getEnv() === 'development',
});

// Adding some custom request interceptor
httpClient.addRequestInterceptor(
  (requestConfig: any) => requestConfig,
  (error: any) => {
    logger.error('Oh we have error in our http request');
    return Promise.reject(error);
  }
);

// Adding some custom response interceptor
httpClient.addResponseInterceptor(
  (response: any) => checkRedirection(response),
  (error: any) => {
    if (
      error &&
      error.response &&
      error.response.data &&
      error.response.data.message
    ) {
      Dialog.warning({
        title: 'Error',
        content: error.response.data.message,
        okLabel: 'OK',
      });
    }

    // Commented this cause it's redirects to not found page event if got API error
    // if (
    //   error.response.status === 404 &&
    //   window.location.pathname !== NotFoundRoutes.path
    // ) {
    //   history.push(NotFoundRoutes.path);
    // }
    return Promise.reject(error);
  }
);

// Setup the default httpClient
export default httpClient;
