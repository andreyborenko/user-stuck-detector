const QueryMapp = {
  limit: '_limit',
  page: '_page',
};

/**
 * A simple middlewares to change the incoming query parameters
 * For example we change query parameter `page` to `_page` so
 * jason-server can understand it
 */
function queryMapper(req, res, next) {
  const params = Object.keys(req.query).map((key) =>
    QueryMapp[key]
      ? { [QueryMapp[key]]: req.query[key] }
      : { [key]: req.query[key] }
  );

  req.query = Object.assign({}, ...params);
  next();
}

/**
 * Modify response header to make sure we dont have CORS issue
 */
function headerModifier(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  // res.header('Access-Control-Request-Headers', 'authorization');
  res.header(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  );
  res.header('Access-Control-Request-Methods', 'GET');
  next();
}

module.exports = [queryMapper, headerModifier];
