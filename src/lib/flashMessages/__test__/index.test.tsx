import React from 'react';
import { mount } from 'enzyme';

const FlashMessages = require('src/lib/flashMessages').default.WrappedComponent;

const setup = (customProps: any = {}) => {
  const defaultProps = {
    dismissFlashMessage: jest.fn(),
    removeFlashMessage: jest.fn(),
    ...customProps,
  };
  const wrapper = mount(<FlashMessages {...defaultProps} />);

  return { props: defaultProps, wrapper };
};

describe('FlashMessages', () => {
  it('should render component without any crash', () => {
    const { wrapper } = setup();
    expect(wrapper).toHaveLength(1);
  });

  it('should be match with snapshot', () => {
    const { wrapper } = setup();
    expect(wrapper).toMatchSnapshot();
  });

  describe('interaction', () => {
    beforeAll(() => {
      window.setTimeout = jest.fn((func) => func());
    });

    it.each([['removeFlashMessage'], ['dismissFlashMessage']])(
      'should trigger `%s` prop function if message is active',
      (functionKey) => {
        const message = { id: 1, isActive: true };
        const mockCallBack = jest.fn();
        const { wrapper } = setup();

        wrapper.setProps({
          [functionKey]: mockCallBack,
          messages: [message],
        });

        expect(mockCallBack).toBeCalledWith({ id: message.id });
      }
    );

    it.each([['removeFlashMessage'], ['dismissFlashMessage']])(
      'should not trigger `%s` prop function if message is inactive',
      (functionKey) => {
        const message = { id: 1, isActive: false };
        const mockCallBack = jest.fn();
        const { wrapper } = setup();

        wrapper.setProps({
          [functionKey]: mockCallBack,
          messages: [message],
        });

        expect(mockCallBack).not.toBeCalled();
      }
    );
  });
});
