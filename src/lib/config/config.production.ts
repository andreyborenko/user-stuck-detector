import { getClientName } from 'src/lib/env';

const ClientName = getClientName();
/**
 * <Dev.Note>
 *  Any parameter that can be different from one environment to another can be stored here.
 *  Any parameter that is defined in this file will be used in production env. You can overide
 *  the config for example for develop env in config.develop.ts
 * </Dev.Note>
 */
export default {
  api: {
    baseUrl: '/v1/learning-api',
    menuUrl: '/nav_resource/api/navigation_json',
    fileUrl: '/api/v0/',
    preferenceUrl: '/user-ui-preference',
  },
  baseName: 'learning',
  assetUrl: '{{LEARNING_CDN}}',
  froalaLicense:
    'OXC1lA3J4B12A10D9D7kOPVi1b1b2Le2e1G1AWMSGSAFDTGHWsE7D7D6E5A1I4E3C3A6D7==',
  launchDarklyKey: '5e60a3c80cd67409b0773213',
  sandcastleUrl: `https://${ClientName}.sandcastle.elmodev.com`,
  // This needs to be updated when there is PROD domain
};
