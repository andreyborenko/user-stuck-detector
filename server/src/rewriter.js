const config = require('./config');
// Use this for define some custom routes and map it to the actual generated one
module.exports = {
  [config.learningApiBaseUrl + '/translations/*']: '/translations',
  [config.learningApiBaseUrl + '/links']: '/subMenu',
  '/nav_resource/api/navigation_json': '/menu',
  '/user-ui-preference*': '/theme',
};
