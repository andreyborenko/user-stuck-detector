import httpClient, { checkRedirection } from 'src/lib/api';
import config from 'src/lib/config';

import { mockEnv } from 'src/lib/test';

mockEnv();

config.api.menuUrl = '/';

jest.mock('src/lib/logger', () => {
  return false;
});

describe('API', () => {
  describe('default', () => {
    it('should setup correctly', () => {
      expect(httpClient.client.defaults.baseURL).toBe(config.api.baseUrl);
      expect(httpClient.client.defaults.useMockData).toBe(true);
    });
  });

  describe('functions', () => {
    it('checkRedirection should return what is passed if request and response are the same.', () => {
      global.window = Object.create(window);
      const dummyURL = 'http://elmodev.com/random/url';
      Object.defineProperty(window, 'location', {
        value: {
          href: dummyURL,
          origin: 'http://elmodev.com',
        },
      });
      const mockResponse = {
        request: {
          responseURL: '/some-api-endpoint',
        },
        config: {
          url: '/some-api-endpoint',
        },
      };
      const result = checkRedirection(mockResponse);
      expect(window.location.href).toBe(dummyURL);
      expect(result).toBe(mockResponse);
    });
  });

  it('checkRedirection should return false if request and response are not the same.', () => {
    global.window = Object.create(window);
    const dummyURL = 'http://elmodev.com/random/url';
    Object.defineProperty(window, 'location', {
      value: {
        href: dummyURL,
        origin: 'http://elmodev.com',
      },
    });
    const mockResponse = {
      request: {
        responseURL: '/some-api-endpoint',
      },
      config: {
        url: '/some-redirection',
      },
    };
    const result = checkRedirection(mockResponse);
    expect(window.location.href).toBe('/learning/my');
    expect(result).toBe(false);
  });
});
