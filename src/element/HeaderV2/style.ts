import styled from 'styled-components';
import { Heading } from 'elmo-elements';

export const HeaderTitle = styled(Heading)`
  margin-right: auto;
  width: 100%;

  @media screen and (min-width: 1024px) {
    flex: 0 1 auto;
    margin-bottom: 0;
  }

  &.header-v2__title {
    white-space: nowrap;
    letter-spacing: -0.3px;
    font-weight: 600 !important;

    @media screen and (min-width: 600px) {
      font-size: 23px !important;
    }
    @media screen and (min-width: 840px) {
      font-size: 28px !important;
      width: auto;
      padding-right: 16px;
    }
  }
`;

export const HeaderAside = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 320px;
  width: 100%;

  @media screen and (max-width: 599.9px) {
    max-width: 120px;
    justify-content: flex-end;
  }
`;

export const HeaderSearchWrapper = styled.div`
  padding: 0;
  max-width: 48px;
  width: 100%;
  margin-right: -16px;
  display: flex;
  align-items: center;

  @media screen and (min-width: 600px) {
    max-width: 320px;
    margin-right: 0;
    margin-left: auto;
  }
`;

export const HeaderWrapper = styled.div`
  display: flex;
  background: #fff;
  align-items: center;
  height: 48px;
  z-index: 2;
  position: relative;
  padding: 0 16px;
  justify-content: space-between;

  @media screen and (min-width: 600px) {
    height: 64px;
    padding: 8px 36px;
  }

  @media screen and (min-width: 840px) {
    padding: 0 44px;
    height: 80px;
  }

  @media screen and (min-width: 1024px) {
    height: 96px;
  }

  @media screen and (max-width: 1279px) {
    margin-top: 48px;
    left: 0;
    z-index: 4;
    width: 100%;
    position: fixed;
  }

  @media screen and (max-width: 840px) {
    justify-content: space-between;
  }
`;

export const HeaderLayout = styled.div`
  height: 194px;
  position: relative;
  &:after {
    content: '';
    position: fixed;
    z-index: 1;
    width: 100%;
    height: 3px;
    left: 0;
    top: 191px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.07), 0 1px 2px rgba(0, 0, 0, 0.14);
  }
  @media screen and (min-width: 500px) {
    height: 160px;
    &:after {
      top: 157px;
    }
  }
  @media screen and (min-width: 600px) {
    height: 176px;
    &:after {
      top: 173px;
    }
  }
  @media screen and (min-width: 839.9px) {
    height: 128px;
    &:after {
      top: 125px;
    }
  }
  @media screen and (min-width: 1024px) {
    height: 144px;
    &:after {
      top: 141px;
    }
  }
  @media screen and (min-width: 1280px) {
    height: 96px;
    &:after {
      position: absolute;
      top: 93px;
    }
  }

  &.header-v2-layout {
    @media screen and (max-width: 839.9px) {
      height: 112px;

      &:after {
        top: 110px;
        z-index: 2;
      }
    }

    @media screen and (max-width: 599.9px) {
      height: 96px;

      &:after {
        top: 93px;
      }
    }
  }
`;
