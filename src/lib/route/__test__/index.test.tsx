import { getCurrentPageKey, getCurrentPageTitle } from 'src/lib/route/';

describe('lib/route', () => {
  it.each([
    ['', '/wrong-path'],
    ['myLearning', '/learning/my-learning'],
    ['courseCatalogue', '/learning/courses'],
    ['manageExternalTraining', '/learning/my-external-training/add'],
    ['editExternalTraining', '/learning/my-external-training/edit/123'],
    ['viewCourseEnrolment', '/learning/view-course-enrolment/123'],
    ['viewMyExternalTraining', '/learning/my-external-training/view/123'],
    ['myExternalTraining', '/learning/my-external-training'],
    ['viewFaceToFaceActivity', '/learning/activity-facetofaces/123'],
    ['viewPageActivity', '/learning/activity-page/123'],
  ])(
    'getCurrentPageKey should return pageKey %s on window.pathname %s',
    (pageKey, pathname) => {
      window.history.pushState({}, '', pathname);
      const currentPageKey = getCurrentPageKey();
      expect(currentPageKey).toBe(pageKey);
    }
  );

  it.each([
    ['', '/wrong-path'],
    ['My Learning', '/learning/my-learning'],
    ['Course Catalogue', '/learning/courses'],
    ['Add External training', '/learning/my-external-training/add'],
    ['Edit External training', '/learning/my-external-training/edit/123'],
    ['View Course Enrolment', '/learning/view-course-enrolment/123'],
    ['View My External Training', '/learning/my-external-training/view/123'],
    ['My External Training', '/learning/my-external-training'],
    ['FaceToFace Activity Sessions List', '/learning/activity-facetofaces/123'],
    ['Page Activity', '/learning/activity-page/123'],
  ])(
    'getCurrentPageTitle should return pageKey %s on window.pathname %s',
    (pageKey, pathname) => {
      window.history.pushState({}, '', pathname);
      const currentPageKey = getCurrentPageTitle();
      expect(currentPageKey).toBe(pageKey);
    }
  );
});

/**
 *
 * myLearning: {
      key: 'pages.myLearning',
      default: 'My Learning',
    },
    courseCatalogue: {
      key: 'pages.courseCatalogue',
      default: 'Course Catalogue',
    },
    manageExternalTraining: {
      key: 'pages.manageExternalTraining',
      default: 'Add External training',
    },
    editExternalTraining: {
      key: 'pages.editExternalTraining',
      default: 'Edit External training',
    },
    viewCourse: {
      key: 'pages.viewCourse',
      default: 'View Course',
    },
    viewCourseEnrolment: {
      key: 'pages.viewCourseEnrolment',
      default: 'View Course Enrolment',
    },
    viewMyExternalTraining: {
      key: 'pages.viewMyExternalTraining',
      default: 'View My External Training',
    },
    myExternalTraining: {
      key: 'pages.myExternalTraining',
      default: 'My External Training',
    },
    viewActivity: {
      key: 'pages.viewActivity',
      default: 'FaceToFace Activity',
    },
 */
