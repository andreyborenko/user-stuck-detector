import { BaseSyntheticEvent } from 'react';

export type PropsType<T> = {
  id?: string;
  tabs: {
    value?: string;
    label: string;
    active?: boolean;
    onClick?: (event: BaseSyntheticEvent) => any;
    ariaLabel?: string;
    to?: string;
  }[];
  filterParamName?: keyof T;
  queryParams?: T;
};
