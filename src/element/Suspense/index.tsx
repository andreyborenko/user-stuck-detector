import React, { Suspense as ReactSuspense, ReactNode } from 'react';
import PageLoading from 'src/element/PageLoading';

export type SuspensePropsType = {
  children: ReactNode;
  fallback?: any;
};

function Suspense({ children, fallback: Fallback }: SuspensePropsType) {
  return (
    <ReactSuspense fallback={Fallback || <PageLoading />}>
      {children}
    </ReactSuspense>
  );
}

export default Suspense;
