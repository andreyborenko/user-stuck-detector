import React from 'react';
import { Redirect, Switch } from 'react-router';
import { RouteType, RedirectRoute, DeafultRoute } from '.';
import PrivateRoute from './PrivateRoute';
import { setPageTitle } from 'src/lib/util';
import { t } from 'src/lib/translation';

function PageTitle({ title }: { title: any }) {
  if (title) {
    setPageTitle(t(title));
  }
  return null;
}

function RouteWithSubRoutes({
  path,
  routes,
  title,
  component: Component,
  routeComponent: RouteComponent = PrivateRoute,
}: DeafultRoute) {
  return (
    <RouteComponent
      path={path}
      render={(props: any) => (
        <>
          <PageTitle title={title} />
          <Component {...props} routes={routes} />
        </>
      )}
    />
  );
}

function NestedRoutes({ routes }: { routes: any }) {
  if (!routes) {
    return null;
  }

  return (
    <Switch>
      {Object.keys(routes).map((key: string, index) => {
        const route: RouteType = routes[key];
        if ((route as RedirectRoute).redirect) {
          const { redirect, path, exact = true } = route as RedirectRoute;
          return (
            <Redirect exact={exact} path={path} to={redirect} key={index} />
          );
        }
        return <RouteWithSubRoutes key={index} {...(route as DeafultRoute)} />;
      })}
    </Switch>
  );
}

export default NestedRoutes;
