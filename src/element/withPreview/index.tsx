import React from 'react';

const previewContextValue = { isPreview: false };
export const PreviewContext = React.createContext(previewContextValue);

export const withPreview = (Component: any) => (props: any) => (
  <PreviewContext.Provider value={{ isPreview: true }}>
    <Component {...props} />
  </PreviewContext.Provider>
);

export default withPreview;
