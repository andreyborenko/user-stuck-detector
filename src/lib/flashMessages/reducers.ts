import { createReducer } from 'redux-act';
import {
  addFlashMessage,
  removeFlashMessage,
  dismissFlashMessage,
} from './actions';
import { FlashMessages, Message } from './types';

export function addFlashMessageReducer(state: FlashMessages, payload: Message) {
  return {
    ...state,
    messages: [...state.messages, { ...payload }],
  };
}

export function dismissFlashMessageReducer(
  state: FlashMessages,
  payload: Message
) {
  const messages = state.messages.map((message: Message) => {
    if (message.id === payload.id) {
      message.isActive = false;
    }
    return message;
  });
  return {
    ...state,
    messages: [...messages],
  };
}

export function removeFlashMessageReducer(
  state: FlashMessages,
  payload: Message
) {
  const messages = state.messages.filter(
    (message: Message) => message.id !== payload.id
  );
  return {
    ...state,
    messages: [...messages],
  };
}

export default createReducer<FlashMessages>(
  {
    [`${addFlashMessage}`]: addFlashMessageReducer,
    [`${removeFlashMessage}`]: removeFlashMessageReducer,
    [`${dismissFlashMessage}`]: dismissFlashMessageReducer,
  },
  {
    messages: [],
  }
);
