declare global {
  interface Window {
    elmolyticsSingleton: any;
  }
}

// Uncomment below to mock elmolyticsSingleton on dev
// window.elmolyticsSingleton = (function() {
//   return {
//     track: function(dataObj: any) {
//       console.log(dataObj);
//     },
//   };
// })();

export default function appAnalytics(
  label: string,
  type = 'New Experience',
  payload: Record<string, any> = {
    learningPage: '',
  },
  product = 'Learning'
) {
  if (window.elmolyticsSingleton && window.elmolyticsSingleton.track) {
    window.elmolyticsSingleton.track({
      product,
      type,
      label,
      payload: {
        learningVersion: 'v2',
        ...payload,
      },
    });
  }
}
