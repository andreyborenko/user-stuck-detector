import translationMapper from 'src/lib/translation/map';

describe('translationMapper', () => {
  it('returns empty object if passed translation data does not match any mapping', () => {
    const result = translationMapper({});
    expect(result).toStrictEqual({});
  });
  it('returns empty object if passed translation data does not match any mapping', () => {
    const mockValue = 'someValue';
    const result = translationMapper({
      cpd: { word: { tabName: mockValue } },
    });
    expect(result).toStrictEqual({
      professionalDevelopment: { title: mockValue },
    });
  });
});
