export const ENV = {
  names: {
    development: 'development',
    staging: 'staging',
    production: 'production',
  },
};

export const PAGINATION = {
  SIZE_OPTIONS: [10, 20, 30],
};

export const CLIENT: string = window.location.hostname.split('.')[0];

export const TRANSLATIONS_KEY = 'AppTranslations';

export const TRANSLATIONS_EXPIRY_TIME = 86400000; // 1day

export const THEME_KEY = 'ElmoTheme';

export const THEME_EXPIRY_TIME = 86400000; // 1day

export type BadgeType =
  | 'grey'
  | 'outlined'
  | 'warning'
  | 'info'
  | 'success'
  | 'danger';

export const MENU_KEY = 'app.menu.stuckDetector';
export const MENU_ADMIN_KEY = 'app.menu.admin';

export const SUBMENU = {
  dashboard: 'app.stuckDetector.submenu.dashboard',
  demo: 'app.stuckDetector.submenu.demo',
};

export const SCREEN_WIDTH_MOBILE = 600;
