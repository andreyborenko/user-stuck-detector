import { all, fork } from 'redux-saga/effects';
import app from './App/sagas';

/**
 * rootSaga
 */
export default function* root() {
  yield all([fork(app)]);
}
