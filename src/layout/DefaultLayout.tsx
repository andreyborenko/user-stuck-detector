import React, { Suspense } from 'react';
import Layout from 'src/element/Layout';
import { NestedRoutes } from 'src/lib/route';
import { MenuWithRouter } from 'src/element/Menu';
import PageLoading from 'src/element/PageLoading';

function DefaultLayout({ routes }: any) {
  return (
    <Suspense fallback={<PageLoading />}>
      <Layout menu={<MenuWithRouter />}>
        {routes && <NestedRoutes routes={routes} />}
      </Layout>
    </Suspense>
  );
}

export default DefaultLayout;
