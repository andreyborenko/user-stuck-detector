export * from './navigation';

export type SagaActionType<T, M = Record<string, any>> = {
  readonly type: string;
  readonly payload: T;
  readonly error: boolean;
  readonly meta?: M;
};

export type QueryParamsType = {
  page: number;
  per_page: number;
  q?: string;
  sort?: string;
};

export type HeaderTabsType = {
  label: string;
  value: string;
};

export type PaginationType = {
  page: number;
  limit: number;
  total: number;
  onPageChange: (page: number) => void;
  onPageSizeChange: (limit: number) => void;
};
