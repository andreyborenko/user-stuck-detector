import React from 'react';
import { Layout, MessageBlock } from 'elmo-elements';
import { t, messages } from 'src/lib/translation';

const NotFound = () => (
  <Layout.Content>
    <div style={{ marginTop: '48px' }}>
      <MessageBlock>
        <MessageBlock.Header>
          {t(messages.app.notFound.title)}
          {t(messages.professionalDevelopment.title)}
        </MessageBlock.Header>
        <MessageBlock.Body>
          {t(messages.app.notFound.content)}
        </MessageBlock.Body>
      </MessageBlock>
    </div>
  </Layout.Content>
);

export default NotFound;
