import React from 'react';
import { Button, Heading, ListTable, Text } from 'elmo-elements';
import { TypographyColor } from 'elmo-elements/Typography/type';
import { compact, map, partial } from 'lodash';

type Props = any;

export const Summary = ({
  onConfirm,
  onCancel,
  onUndo,
  changedUsers = [],
}: Props) => {
  const actions: {
    [key: string]: { label: string; color: TypographyColor };
  } = {
    'select-option': { label: 'Added', color: 'success' },
    'remove-value': { label: 'Removed', color: 'danger' },
  };
  return (
    <>
      <Heading className="mb-3">User Details</Heading>

      {!changedUsers.length && <div className="mb-3">No change Found</div>}

      {!!changedUsers.length && (
        <ListTable ariaLabel="ariaLabel" id="SummaryListTable" isSticky={true}>
          <ListTable.Header>
            <ListTable.Column label="User Name" />
            <ListTable.Column label="Status" />
            <ListTable.Column label="" />
          </ListTable.Header>
          <ListTable.Body>
            {map(changedUsers, ({ id, name, action }: any) => {
              const { color, label } = actions[action];
              return (
                <ListTable.Tr key={id}>
                  {compact([
                    name,
                    <Text color={color}>{label}</Text>,
                    <Button onClick={partial(onUndo, id, action)}>Undo</Button>,
                  ]).map((content, key) => (
                    <ListTable.Td key={`${id}-${key}`}>{content}</ListTable.Td>
                  ))}
                </ListTable.Tr>
              );
            })}
          </ListTable.Body>
        </ListTable>
      )}

      {!!changedUsers.length && (
        <div className="mt-4 d-flex flex-justify-content-center">
          <Button className="mr-2" onClick={onConfirm} type="primary">
            Confirm
          </Button>
          <Button onClick={onCancel} type="primary">
            Cancel
          </Button>
        </div>
      )}
    </>
  );
};
