export default class ErrorAppInitUndefinedError extends Error {
  constructor(...params: any) {
    super(...params);

    this.name = 'ErrorAppInitUndefinedError';
    this.message = this.message || 'Undefined error';
  }
}
