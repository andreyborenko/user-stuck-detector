import { rootReducer } from './store';

export type ReduxStoreType = ReturnType<typeof rootReducer>;
