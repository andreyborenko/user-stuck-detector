import { getEnv } from 'src/lib/env';
import { ErrorConfigUndefined } from 'src/lib/error';

export function getGlobalConfig() {
  const { learningApp } = window;
  let globalConfig = {};
  if (learningApp) {
    // This maps the global config to FE app config(config.ENV.ts)
    globalConfig = {
      ...(learningApp.ldClientId && {
        launchDarklyKey: learningApp.ldClientId,
      }),
    };
  }

  return globalConfig;
}

/**
 * Get the config based on the env and merge it with configs comming from server
 * <Dev.Note>
 *  This function should be in sync mode. If you have any async operation you should
 *  do it at app initialisation time and you need to keep it in app state. For example
 *  the 'loader.ts' is for this perpose.
 * </Dev.Note>
 */
export function getConfig() {
  const env = getEnv();
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const config = require(`./config.${env}`).default;

  // <Dev.Note>
  //  Try to have some custom errors rather than just throwing a general Error
  // </Dev.Note>
  if (!config) {
    throw new ErrorConfigUndefined();
  }

  // Global config will override config.ENV.ts
  const globalConfig = getGlobalConfig();

  return {
    ...config,
    ...globalConfig,
  };
}

// This needs to be updated if we add more config on global
type learningAppType = {
  ldClientId?: string;
};
declare global {
  interface Window {
    learningApp: learningAppType;
  }
}

export default getConfig();
