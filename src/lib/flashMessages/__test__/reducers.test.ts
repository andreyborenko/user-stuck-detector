import * as reducers from 'src/lib/flashMessages/reducers';
import { FlashMessages, Message } from 'src/lib/flashMessages/types';

const baseTestFlashMessage: Message = {
  isActive: true,
  type: 'info',
  message: 'test message',
  onClose: jest.fn(),
};
const testFlashMessage1: Message = { ...baseTestFlashMessage, id: '1' };
const testFlashMessage2: Message = { ...baseTestFlashMessage, id: '2' };

const mockState: FlashMessages = {
  messages: [testFlashMessage1, testFlashMessage2],
};

describe('reducers', () => {
  it('addFlashMessageReducer should return default state', () => {
    expect(
      reducers.addFlashMessageReducer(mockState, baseTestFlashMessage)
    ).toEqual({
      ...mockState,
      messages: [...mockState.messages, baseTestFlashMessage],
    });
  });

  it('dismissFlashMessageReducer should return default state', () => {
    expect(
      reducers.dismissFlashMessageReducer(mockState, testFlashMessage2)
    ).toEqual({
      ...mockState,
      messages: [testFlashMessage1, { ...testFlashMessage2, isActive: false }],
    });
  });

  it('removeFlashMessageReducer should return default state', () => {
    expect(
      reducers.removeFlashMessageReducer(mockState, testFlashMessage2)
    ).toEqual({
      ...mockState,
      messages: [testFlashMessage1],
    });
  });
});
