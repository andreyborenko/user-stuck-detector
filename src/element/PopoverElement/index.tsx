import React, { ReactNode } from 'react';
import { Popover } from 'elmo-elements';
import { PopoverProps } from 'elmo-elements/Popover/type';

type PopoverElementType = Partial<PopoverProps> & {
  target: ReactNode;
  content: ReactNode;
};

// TODO: Add unit tests

export const PopoverElement = ({
  target,
  content,
  ...props
}: PopoverElementType) => (
  <Popover {...props}>
    <Popover.Target>{target}</Popover.Target>
    <Popover.Content>{content}</Popover.Content>
  </Popover>
);

export default PopoverElement;
