import moment, { unitOfTime } from 'moment';
import momentTimezone from 'moment-timezone';
import { messages, t } from 'src/lib/translation';

export const DATE_FORMAT = 'D MMM YYYY';

export function dateFormat(date: moment.MomentInput, format?: string) {
  if (!date) {
    return undefined;
  }
  return moment(date).format(format || DATE_FORMAT);
}

export function getCurrentUnixDate() {
  return moment().unix();
}

export function unixDate(date: number) {
  return moment.unix(date);
}

export function unixDateFormat(date: number, format?: string): string {
  if (!date) {
    return '';
  }

  return unixDate(date).format(format || DATE_FORMAT);
}

export function timestampDiff(date1: number, date2: number) {
  const duration = moment.duration(unixDate(date1).diff(unixDate(date2)));
  return duration.asHours();
}

export function dateDiff(
  date1: moment.MomentInput,
  date2: moment.MomentInput,
  format?: unitOfTime.Diff
) {
  const date1Moment = moment(date1);
  const date2Moment = moment(date2);
  return date1Moment.diff(date2Moment, format);
}

export function formatTimestampWithTimezone(
  timestamp: number,
  timezone: string,
  format: string
) {
  return momentTimezone.unix(timestamp).tz(timezone).format(format);
}

export function getCurrentDateWithTimezone(timezone: string, format: string) {
  return momentTimezone.tz(timezone).format(format);
}

