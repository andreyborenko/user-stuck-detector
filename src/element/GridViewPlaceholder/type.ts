export type DashboardColResponsiveOptionType = number | Record<string, any>;

export type GridViewPlaceholderPropsType = {
  /** id attribute, optional id */
  id?: string;
  /** Number of cols, the default is 3 */
  cols?: number;
  /** Number of rows, the default is 3 */
  rows?: number;
  /** xs responsive atribute of col */
  xs?: DashboardColResponsiveOptionType;
  /** sm responsive atribute of col */
  sm?: DashboardColResponsiveOptionType;
  /** md responsive atribute of col */
  md?: DashboardColResponsiveOptionType;
  /** lg responsive atribute of col */
  lg?: DashboardColResponsiveOptionType;
  /** xl responsive atribute of col */
  xl?: DashboardColResponsiveOptionType;
  /** xxl responsive atribute of col */
  xxl?: DashboardColResponsiveOptionType;
  /** xxxl responsive atribute of col */
  xxxl?: DashboardColResponsiveOptionType;
};
