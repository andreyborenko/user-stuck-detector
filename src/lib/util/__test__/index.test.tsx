import React from 'react';
import { toUpper } from 'lodash';
import { DefalutImages } from 'src/lib/const';
import history from 'src/lib/history';
import {
  getComponentName,
  withDelay,
  getRoute,
  setPageTitle,
  getLocalstorage,
  setLocalstorage,
  getSessionstorage,
  setSessionstorage,
  getDefaultImage,
  getExtensionsByMimeTypes,
  parseDecimal,
  checkNaN,
  checkNegative,
  checkMaxValue,
  getTimeFromNum,
  getDurationInHoursAndMinutes,
  replaceFirstSubtractValue,
  replaceFirstValue,
  sanitizeHTML,
  isSignUpUpcoming,
  isEnrolmentOverdue,
  getFullViewModeName,
  isFieldInvalid,
  handleHeaderTabChange,
} from 'src/lib/util';

test('getComponentName', () => {
  expect(getComponentName(<div />)).toBe('Component');
});

describe('withDelay', () => {
  const callback = jest.fn(() => 1);

  it('should work properly', async () => {
    await withDelay(callback, 100);
    expect(callback).toHaveBeenCalledTimes(1);
  });
});

describe('getRoute', () => {
  it('should formatting exact path with params', () => {
    expect(
      getRoute('/users/user/:id', { params: { id: 12 }, query: { page: 12 } })
    ).toBe(`/users/user/12?page=12`);
  });

  it('should formatting exact path without params', () => {
    expect(getRoute('/users/users')).toBe('/users/users');
  });
});

test('setPageTitle', () => {
  const mockedTitle = 'mocked title';
  expect(typeof document).toBe('object');
  expect(document).toHaveProperty('title');
  setPageTitle(mockedTitle);
  expect(document.title).toBe(mockedTitle);
});

const key = 'key';
const valueObject = { innerKey: 'innerValue' };
const mockedObjWithString = { [key]: 'value' };
const mockedObjWithObject = { [key]: JSON.stringify(valueObject) };

describe('getLocalstorage', () => {
  it('should return false if localStorage key is unavailable', () => {
    expect(getLocalstorage(key)).toBe(false);
  });

  it('should return string if localStorage key is available', () => {
    localStorage.setItem(key, mockedObjWithString[key]);
    expect(getLocalstorage(key)).toBe(mockedObjWithString[key]);
  });

  it('should return object if localStorage key is available', () => {
    localStorage.setItem(key, mockedObjWithObject[key]);
    expect(getLocalstorage(key, 'object')).toEqual(valueObject);
  });
});

describe('setLocalstorage', () => {
  it('should write string into localStorage', () => {
    setLocalstorage(key, mockedObjWithString[key]);
    expect(getLocalstorage(key)).toBe(mockedObjWithString[key]);
  });

  it('should write string into localStorage', () => {
    setLocalstorage(key, mockedObjWithObject[key]);
    expect(getLocalstorage(key)).toBe(mockedObjWithObject[key]);
  });
});

describe('getSessionstorage', () => {
  it('should return false if localStorage key is unavailable', () => {
    expect(getSessionstorage(key)).toBe(false);
  });

  it('should return string if localStorage key is available', () => {
    sessionStorage.setItem(key, mockedObjWithString[key]);
    expect(getSessionstorage(key)).toBe(mockedObjWithString[key]);
  });

  it('should return object if localStorage key is available', () => {
    sessionStorage.setItem(key, mockedObjWithObject[key]);
    expect(getSessionstorage(key, 'object')).toEqual(valueObject);
  });
});

describe('setSessionstorage', () => {
  it('should write string into localStorage', () => {
    setSessionstorage(key, mockedObjWithString[key]);
    expect(getSessionstorage(key)).toBe(mockedObjWithString[key]);
  });

  it('should write string into localStorage', () => {
    setSessionstorage(key, mockedObjWithObject[key]);
    expect(getSessionstorage(key)).toBe(mockedObjWithObject[key]);
  });
});

describe('getDefaultImage', () => {
  it.each([
    ['image url', 'some-image-url', '1000', 'some-image-url'],
    ['default aqua image', '', '1000', DefalutImages[0]],
    ['default green image', '', '1001', DefalutImages[1]],
    ['default blue image', '', '1002', DefalutImages[2]],
    ['default purple image', '', '1003', DefalutImages[3]],
    ['default yellow image', '', '1004', DefalutImages[4]],
    ['default aqua image', '', '1005', DefalutImages[0]],
    ['default green image', '', '1006', DefalutImages[1]],
    ['default blue image', '', '1007', DefalutImages[2]],
    ['default purple image', '', '1008', DefalutImages[3]],
    ['default yellow image', '', '1009', DefalutImages[4]],
  ])('should return %s', (description, image, categoryId, expected) => {
    expect(getDefaultImage(image, categoryId)).toBe(expected);
  });
});

describe('getExtensionsByMimeTypes', () => {
  it.each([
    ['empty array', [], []],
    ['extension', ['image/jpeg'], ['jpeg']],
  ])('should return %s', (_, mimeTypes, expected) => {
    expect(getExtensionsByMimeTypes(mimeTypes)).toEqual(expected);
  });

  it('should return formatted extension', () => {
    const mimeTypes = ['image/jpeg'];
    const expected = ['JPEG'];
    expect(getExtensionsByMimeTypes(mimeTypes, toUpper)).toEqual(expected);
  });
});

describe('getFormattedDuration', () => {
  const mockedDuration = '1h 30m';
  const mockedDuration1 = '45m';
  const mockedDuration2 = '10h ';

  it('should return correct duration string with 1h 30m', () => {
    expect(getTimeFromNum(1.5)).toEqual(mockedDuration);
  });

  it('should return correct duration string with 30m', () => {
    expect(getTimeFromNum(0.75)).toEqual(mockedDuration1);
  });

  it('should return correct duration string with 10h', () => {
    expect(getTimeFromNum(10)).toEqual(mockedDuration2);
  });
});

describe('getFormattedDurationInHoursAndMinutes', () => {
  const mockedDuration = '1h 30m';
  const mockedDuration1 = '45m';
  const mockedDuration2 = '51h 30m';

  it('should return correct duration string with 1h 30m', () => {
    expect(getDurationInHoursAndMinutes(1.5)).toEqual(mockedDuration);
  });

  it('should return correct duration string with 30m', () => {
    expect(getDurationInHoursAndMinutes(0.75)).toEqual(mockedDuration1);
  });

  it('should return correct duration string with 51h 30m', () => {
    expect(getDurationInHoursAndMinutes(51.5)).toEqual(mockedDuration2);
  });
});

describe('number utils should work properly', () => {
  it('parseDecimal', () => {
    // proper work
    expect(parseDecimal('1.01')).toBeCloseTo(1.01);
    expect(parseDecimal('1')).toBeCloseTo(1.0);
    // invalid
    expect(parseDecimal('hello world')).toBeNaN();
  });

  it('checkNaN', () => {
    expect(checkNaN(+'hello world')).toBe(0);
    expect(checkNaN(1)).toBe(1);
  });

  it('checkNegative', () => {
    expect(checkNegative(-1)).toBe(0);
    expect(checkNegative(1)).toBe(1);
  });

  it('checkMaxValue', () => {
    expect(checkMaxValue(1, 2)).toBe(1);
    expect(checkMaxValue(2, 2)).toBe(2);
    expect(checkMaxValue(3, 2)).toBe(0);
  });
});

describe('replaceFirstSubtractValue', () => {
  it('should return text without first subtract char', () => {
    const text = '-test text';
    expect(replaceFirstSubtractValue(text)).toEqual('test text');
  });

  it('should return text', () => {
    const text = 'test-text';
    expect(replaceFirstSubtractValue(text)).toEqual(text);
  });
});

describe('replaceFirstValue', () => {
  it('should return text with replaced first char', () => {
    const text = 'test text';
    expect(replaceFirstValue(text, 't', 'm')).toEqual('mest text');
  });

  it('should return text', () => {
    const text = 'test text';
    expect(replaceFirstValue(text, 'w', 'm')).toEqual(text);
  });
});

describe('sanitizeHTML', () => {
  it.each([
    [
      'remove script tag',
      '<script>dummy_script_to_be_removed</script>',
      (result: string) => {
        return result === '';
      },
    ],
    [
      'allow iframe tag',
      '<iframe src="#" />',
      (result: string) => {
        return result.indexOf('iframe') !== -1;
      },
    ],
    [
      'keep attributes',
      '<a target="_blank" href="#" allow allowfullscreen frameborder scrolling>dummy link text</a>',
      (result: string) => {
        return (
          result.indexOf('target') !== -1 &&
          result.indexOf('allow') !== -1 &&
          result.indexOf('allowfullscreen') !== -1 &&
          result.indexOf('frameborder') !== -1 &&
          result.indexOf('scrolling') !== -1
        );
      },
    ],
  ])(
    'should sanitize html string - %s',
    (message, stringProvided, resultExpected) => {
      const result = sanitizeHTML(stringProvided);
      const resultToBe = resultExpected(result);
      expect(resultToBe).toBeTruthy();
    }
  );
});

describe('isSignUpUpcoming', () => {
  it('should return false if sign up is archived', () => {
    const signUp = {
      archived: true,
      facetoface_session: {
        session_end_timestamp: 9999999999,
        facetoface_session_times: [{}],
      },
    };
    expect(isSignUpUpcoming(signUp)).toBe(false);
  });

  it('should return false if current date is more than session end date', () => {
    const signUp = {
      archived: false,
      facetoface_session: {
        session_end_timestamp: 1111111111,
        facetoface_session_times: [{}],
      },
    };
    expect(isSignUpUpcoming(signUp)).toBe(false);
  });

  it('should return false if a session has date', () => {
    const signUp = {
      archived: false,
      facetoface_session: {
        session_end_timestamp: 1111111111,
        facetoface_session_times: [{}],
      },
    };
    expect(isSignUpUpcoming(signUp)).toBe(false);
  });

  it('should return true if current date is less than session end date', () => {
    const signUp = {
      archived: false,
      facetoface_session: {
        session_end_timestamp: 9999999999,
        facetoface_session_times: [{}],
      },
    };
    expect(isSignUpUpcoming(signUp)).toBe(true);
  });

  it('should return true if a session does not have date', () => {
    const signUp = {
      archived: false,
      facetoface_session: {
        session_end_timestamp: 1111111111,
        facetoface_session_times: [],
      },
    };
    expect(isSignUpUpcoming(signUp)).toBe(true);
  });
});

describe('isEnrolmentOverdue', () => {
  const enrolment = {
    due_timestamp: 1111111111,
    status: '1',
  };

  it('should return true', () => {
    expect(isEnrolmentOverdue(enrolment)).toBe(true);
  });

  it.each([
    [
      'due timestamp does not exist',
      { ...enrolment, due_timestamp: undefined },
    ],
    [
      'due timestamp is more than now',
      { ...enrolment, due_timestamp: 9999999999 },
    ],
    ['enrolment status is 2', { ...enrolment, status: '2' }],
  ])('should return false if %s', (_, data) => {
    expect(isEnrolmentOverdue(data)).toBe(false);
  });
});

describe('getFullViewModeName', () => {
  it.each([
    ['grid', 'Grid View'],
    ['list', 'List View'],
  ])('should return full view mode name', (mode, expected) => {
    expect(getFullViewModeName(mode)).toEqual(expected);
  });
});

describe('isFieldInvalid', () => {
  it.each([
    ['', true, true, /^[0-9]*$/],
    ['', false, false, /^[0-9]*$/],
    ['1', true, false, /^[0-9]*$/],
    ['1', false, false, /^[0-9]*$/],
    ['a', false, true, /^[0-9]*$/],
    ['a', true, true, /^[0-9]*$/],
  ])(
    'when value is `%s` and field is %s should return %s',
    (value, isFieldRequired, expected, regExp) => {
      expect(isFieldInvalid(value, isFieldRequired, regExp)).toBe(expected);
    }
  );
});

describe('handleHeaderTabChange', () => {
  it.each([['testTab1'], ['testTab2']])(
    'should change selectedHeaderTab to %s',
    (value) => {
      const spy = jest.spyOn(history, 'push');
      handleHeaderTabChange(value);
      expect(spy).toBeCalledWith(`/?selectedHeaderTab=${value}`);
    }
  );
});
