import {
  default as NavigationService,
  menuHttpClient,
} from 'src/element/Menu/service';
import { Navigation } from 'src/element/Menu/model';
import { HttpClient } from 'elmo-react-core';
import config from 'src/lib/config';
import httpClient from 'src/lib/api';

import { mockEnv } from 'src/lib/test';

mockEnv();

jest.mock('elmo-react-core');

config.api.menuUrl = '/';

jest.mock('src/lib/logger', () => {
  return false;
});

const subMenuApiData = [
  {
    rel: 'CourseEnrolment',
  },
  {
    rel: 'Course',
  },
  {
    rel: 'ExternalTrainingEnrolment',
  },
  {
    rel: 'CpdPlan',
  },
  {
    rel: 'QuizSubmissionMarking',
  },
  {
    rel: 'CourseEnrolmentRequest',
  },
];

const menuData = {
  user: {
    id: 145,
    name: 'Luci Employee',
    img:
      'https://learningapi.dev.elmodev.com/media/cache/resolve/thumb_40_40/files/learningapi//i/mg//img/user-default-picture.jpeg',
  },
  branding: {
    logo:
      'https://assets.elmosoftware.com.au/wp-content/themes/elmoone/img/elmo_logoNEW_CMYK_hrpayroll.png',
    smallLogo: '/elmo_favicon.png',
    logoLink: '/',
    companyName: 'learningapi',
    version: '',
    theme: {
      // profile: { color: '#428bca', backgroundColor: '#ffffff' },
      // admin: { color: '#000000', backgroundColor: '#ffffff' },
      // menu: {
      //   backgroundColor: '#195799',
      //   fontColor: '#ffffff',
      //   hoverFontColor: '#ffffff',
      //   font: 'Arial, \u0022Helvetica Neue\u0022, Helvetica, sans-serif',
      // },
    },
  },
  menu: {
    user: [
      {
        key: 'user.action.switchBack',
        icon: 'SwitchAccountsIcon',
        title: 'Switch to my user',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/switch-user?_login_as=_exit',
        children: [],
      },
      {
        key: 'app.menu.changePassword',
        icon: 'ChangePasswordIcon',
        title: 'Change Password',
        description: '',
        url:
          'https://learningapi.dev.elmodev.com/controlpanel/change-password/',
        children: [],
      },
      {
        key: 'app.menu.signOut',
        icon: 'SignOutIcon',
        title: 'Sign Out',
        description: '',
        url: '/logout',
        children: [],
      },
    ],
    modules: [
      {
        key: 'app.menu.home',
        icon: 'elmo-icon-home',
        title: 'Home',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/dashboard',
        children: [],
      },
      {
        key: 'app.menu.myProfile',
        icon: 'elmo-icon-my-profile',
        title: 'Profile',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/controlpanel/my-profile',
        children: [],
      },
      {
        key: 'app.menu.myEmployees',
        icon: 'elmo-icon-my-employees',
        title: 'My Team',
        description: '',
        url:
          'https://learningapi.dev.elmodev.com/controlpanel/employees-search',
        children: [],
      },
      {
        key: 'app.menu.learning',
        icon: 'elmo-icon-learning',
        title: 'Learning',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/learning/my',
        children: [],
      },
      {
        key: 'app.menu.performance',
        icon: 'elmo-icon-performance',
        title: 'Performance',
        description: '',
        url: 'https://learningapi.dev.elmodev.com/dashboard/my-performance',
        children: [],
      },
    ],
  },
  status: { location: { key: null, title: null } },
};

const errorMessage = 'Received data is not valid';

describe('Menu Service', () => {
  describe('rendering', () => {
    it('should set menuHttpClient', () => {
      expect(HttpClient).toBeCalledTimes(2);
    });

    it('should run getInitState', () => {
      const navigationService = NavigationService.getInitState();
      const navigation = new Navigation();

      expect(navigationService).toEqual({
        error: undefined,
        navigation: navigation,
        loaded: false,
      });
    });

    it('should getData return error message if API result does not meet what it should return', async () => {
      menuHttpClient.get.mockResolvedValue({ something: 'wrong' });
      let result = await NavigationService.getData().catch((response) => {
        expect(response).toBe(errorMessage);
      });
    });

    it('should getData returns success if API success', async () => {
      menuHttpClient.get.mockResolvedValue({ data: menuData });
      let result = await NavigationService.getData();
      expect(result).toEqual(Object.assign(new Navigation(), menuData));
    });

    it('should run getData false if API failure', async () => {
      menuHttpClient.get.mockRejectedValue({ success: false, error: 'error' });
      let result = await NavigationService.getData().catch((response) => {
        expect(response).toBe(errorMessage);
      });
    });

    it('should getLearningSubMenuData return error message if API result does not meet what it should return', async () => {
      httpClient.get.mockResolvedValue({ something: 'wrong' });
      let result = await NavigationService.getLearningSubMenuData().catch(
        (response) => {
          expect(response).toBe(errorMessage);
        }
      );
    });

    it('should getLearningSubMenuData returns success if API success', async () => {
      httpClient.get.mockResolvedValue({ data: subMenuApiData });
      let result = await NavigationService.getLearningSubMenuData();
      expect(result).toEqual(
        Object.assign(NavigationService.mapResourcetoMenuItems(subMenuApiData))
      );
    });

    it('should run getLearningSubMenuData false if API failure', async () => {
      httpClient.get.mockRejectedValue({
        success: false,
        error: 'error',
      });
      let result = await NavigationService.getLearningSubMenuData().catch(
        (response) => {
          expect(response).toBe(errorMessage);
        }
      );
    });
  });
});
