import { getSessionstorage, setSessionstorage } from 'src/lib/util';
import config from 'src/lib/config';
import { THEME_KEY } from 'src/lib/const';
import {
  themeHttpClient,
  fetchTheme,
  loadTheme,
  getTheme,
  mapTheme,
} from 'src/lib/theme';
import { defaultTheme } from 'elmo-elements';

jest.mock('elmo-react-core');

jest.mock('src/lib/config');
config.api.menuUrl = '/';

const fakeFetchData = {
  siteFont:
    '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol',
  headingBannerBgColor: '#ffffff',
  menuColor: '#007eb1',
  menuFontColor: '#ffffff',
  menuHoverFontColor: '#ffffff',
  linkColor: '#ff0000',
  buttonColor: '#19579f',
  buttonHoverColor: '#1f5997',
  buttonBorderColor: '#007eb1',
  buttonFontColor: '#ffffff',
  tableHeaderColor: '#007eb1',
  tableHeaderFontColor: '#ffffff',
  adminBgColor: '#ffffff',
  adminColor: '#000000',
  headingBannerUserMenuColor: '#428bca',
};
const fakeSessionstorageData = { someKey: 'some data in sessionstorage' };

describe('lib/theme', () => {
  describe('getTheme', () => {
    it('should return default theme if there is not theme stored in sessionstorage', async () => {
      const fakeThemeData = {};
      setSessionstorage(
        THEME_KEY,
        {
          timestamp: Date.now(),
          data: fakeThemeData,
        },
        'object'
      );
      const result = await getTheme();
      expect(result).toStrictEqual(defaultTheme);
    });
    it('should return theme overridden if there is theme stored in sessionstorage', async () => {
      const fakeThemeData = { font: 'serif' };
      setSessionstorage(
        THEME_KEY,
        {
          timestamp: Date.now(),
          data: fakeThemeData,
        },
        'object'
      );
      const result = await getTheme();
      expect(result).toStrictEqual({ ...defaultTheme, ...fakeThemeData });
    });
  });

  describe('fetchTheme', () => {
    const errorMessage = new Error('error');

    it('should return error if API fail', async () => {
      themeHttpClient.get.mockRejectedValue({ success: false, error: 'error' });
      let result = await fetchTheme().catch((response) => {
        expect(JSON.stringify(response)).toBe(JSON.stringify(errorMessage));
      });
    });

    it('should return error if API success but no data', async () => {
      themeHttpClient.get.mockResolvedValue({ something: 'wrong' });
      let result = await fetchTheme().catch((response) => {
        expect(JSON.stringify(response)).toBe(JSON.stringify(errorMessage));
      });
    });

    it('should return server respnose.data if success', async () => {
      themeHttpClient.get.mockResolvedValue({ data: fakeFetchData });
      let result = await fetchTheme();
      await expect(result).toStrictEqual(mapTheme(fakeFetchData));
    });
  });

  describe('loadTheme', () => {
    it('should not fetch if the theme timestamp is not expired', async () => {
      setSessionstorage(
        THEME_KEY,
        {
          timestamp: Date.now(),
          data: fakeSessionstorageData,
        },
        'object'
      );
      const result = await loadTheme();
      const storedTheme = getSessionstorage(THEME_KEY, 'object');
      expect(storedTheme.data).toStrictEqual(fakeSessionstorageData);
    });

    it('should fetch if the theme timestamp is expired', async () => {
      setSessionstorage(
        THEME_KEY,
        {
          timestamp: 0,
          data: fakeSessionstorageData,
        },
        'object'
      );
      const result = await loadTheme();
      const storedTheme = getSessionstorage(THEME_KEY, 'object');
      await expect(storedTheme.data).toStrictEqual(mapTheme(fakeFetchData));
    });
  });
});
