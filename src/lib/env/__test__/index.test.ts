import { getClientName } from '../';

const mockHostname = (oldWindowLocation, hostname) => {
  delete window.location;
  window.location = Object.defineProperties(
    {},
    {
      ...Object.getOwnPropertyDescriptors(oldWindowLocation),
      hostname: {
        configurable: true,
        value: hostname,
      },
    }
  );
};

describe('env', () => {
  describe('getClientName', () => {
    const oldWindowLocation = window.location;

    const mockSubdomain = 'abc';

    beforeEach(() => {
      mockHostname(oldWindowLocation, mockSubdomain + '.elmodev.com');
    });

    afterEach(() => {
      window.location = oldWindowLocation;
    });

    it('return subdomain correctly', () => {
      const clientName = getClientName();
      expect(clientName).toBe(mockSubdomain);
    });
  });

  describe('getClientName', () => {
    const oldWindowLocation = window.location;

    const mockSubdomain = '';

    beforeEach(() => {
      mockHostname(oldWindowLocation, '');
    });

    afterEach(() => {
      window.location = oldWindowLocation;
    });

    it('return empty string without error if window.location.hostname returns empty string', () => {
      const clientName = getClientName();
      expect(clientName).toBe(mockSubdomain);
    });
  });
});
