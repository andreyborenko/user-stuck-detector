const config = require('../config');

const menu = {
  user: {
    id: 145,
    name: 'Employee',
    img: 'http://localhost:' + config.port + '/images/avatar.jpg',
  },
  branding: {
    logo: 'http://localhost:' + config.port + '/images/logo.png',
    smallLogo: 'http://localhost:' + config.port + '/images/logo-small.png',
    logoLink: '/',
    companyName: '',
    version: '',
    theme: {
      // profile: { color: '#428bca', backgroundColor: '#ffffff' },
      // admin: { color: '#000000', backgroundColor: '#ffffff' },
      // menu: {
      //   backgroundColor: '#195799',
      //   fontColor: '#ffffff',
      //   hoverFontColor: '#ffffff',
      //   font: 'Arial, \u0022Helvetica Neue\u0022, Helvetica, sans-serif',
      // },
    },
  },
  menu: {
    user: [
      {
        key: 'user.action.switchBack',
        icon: 'SwitchAccountsIcon',
        title: 'Switch to my user',
        description: '',
        url: '/switch-user?_login_as=_exit',
        children: [],
      },
      {
        key: 'app.menu.changePassword',
        icon: 'ChangePasswordIcon',
        title: 'Change Password',
        description: '',
        url: '/controlpanel/change-password/',
        children: [],
      },
      {
        key: 'app.menu.signOut',
        icon: 'SignOutIcon',
        title: 'Sign Out',
        description: '',
        url: '/logout',
        children: [],
      },
    ],
    modules: [
      {
        key: 'app.menu.home',
        icon: 'elmo-icon-home',
        title: 'Home',
        description: '',
        url: '/dashboard',
        children: [],
      },
      {
        key: 'app.menu.myProfile',
        icon: 'elmo-icon-my-profile',
        title: 'Profile',
        description: '',
        url: '/controlpanel/my-profile',
        children: [],
      },
      {
        key: 'app.menu.myEmployees',
        icon: 'elmo-icon-my-employees',
        title: 'My Team',
        description: '',
        url: '/controlpanel/employees-search',
        children: [],
      },
      {
        key: 'app.menu.learning',
        icon: 'elmo-icon-learning',
        title: 'My Learning',
        description: '',
        url: '/learning/my-learning',
        children: [],
      },
      {
        key: 'app.menu.stuckDetector',
        icon: 'elmo-icon-search',
        title: 'Stuck Detector',
        description: '',
        url: '/stuck-detector/dashboard',
        children: [],
      },
      {
        key: 'app.menu.performance',
        icon: 'elmo-icon-performance',
        title: 'Performance',
        description: '',
        url: '/dashboard/my-performance',
        children: [],
      },
    ],
  },
  status: { location: { key: null, title: null } },
};

module.exports = { menu };
