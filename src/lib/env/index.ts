import { ENV } from 'src/lib/const';
import config from 'src/lib/config';

/**
 * Some logic to get the env name
 */
export function getEnv() {
  return process.env.REACT_APP_BUILD_ENV;
}

/**
 *
 * @param env
 */
export function isDev(env?: any) {
  if (!env) {
    env = getEnv();
  }

  return env && env === ENV.names.development;
}

/**
 * get Sandcastle URL
 */
export function getSandcastleURL() {
  return config.sandcastleUrl;
}

/**
 * get clientname from hostname
 */
export function getClientName() {
  return window.location.hostname.split('.')[0];
}
