import React from 'react';
import styled from 'styled-components';
import { getClass } from 'src/lib/util';

type IconElementProps = IconWrapperType & {
  size?: 'md' | 'sm' | 'xs';
  className?: string;
  title?: string;
  children: React.ReactNode;
};

const IconElementDefaultProps = {
  size: 'md',
  className: undefined,
  title: undefined,
  color: undefined,
};

type IconWrapperType = {
  color?: string;
};

const IconWrapper = styled.span<IconWrapperType>`
  color: ${({ color }) => color};
`;

function IconElement({
  size,
  title,
  className,
  children,
  color,
}: IconElementProps) {
  return (
    <IconWrapper
      color={color}
      data-testid="icon-element"
      title={title}
      className={getClass('elmo-icon', className, { [`size-${size}`]: size })}
    >
      <svg
        focusable="false"
        viewBox="0 0 24 24"
        aria-hidden="true"
        role="presentation"
        className="elmo-icon__svg"
      >
        {children}
      </svg>
    </IconWrapper>
  );
}

IconElement.defaultProps = IconElementDefaultProps;

export default IconElement;
export * from './component';
