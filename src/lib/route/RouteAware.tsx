import React from 'react';
import NestedRoutes from './NestedRoutes';
import { getComponentName } from 'src/lib/util';

/**
 *
 * @param Component
 */
function withRouteAware(Component: any) {
  const RouteAware = (props: any) => {
    const { routes } = props;
    return (
      <>
        {/* We might need to have a props like `justModals` to only select the modal routes */}
        <NestedRoutes routes={routes} />
        <Component {...props} />
      </>
    );
  };

  RouteAware.displayName = `RouteAware(${getComponentName(Component)})`;

  return RouteAware;
}

export default withRouteAware;
