const config = require('../config');

const theme = {
  siteFont:
    '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol',
  headingBannerBgColor: '#ffffff',
  menuColor: '#19579f',
  menuFontColor: '#ffffff',
  menuHoverFontColor: '#ffffff',
  linkColor: '#ff0000',
  buttonColor: '#ff0000',
  buttonHoverColor: '#ff0000',
  buttonBorderColor: '#007eb1',
  buttonFontColor: '#ffffff',
  tableHeaderColor: '#007eb1',
  tableHeaderFontColor: '#ffffff',
  adminBgColor: '#ffffff',
  adminColor: '#000000',
  headingBannerUserMenuColor: '#545454',
};

module.exports = { theme };
