export default class ErrorConfigUndefined extends Error {
  constructor(...params: any) {
    super(...params);

    this.name = 'ErrorConfigUndefined';
    this.message = this.message || 'Config is undefined';
  }
}
