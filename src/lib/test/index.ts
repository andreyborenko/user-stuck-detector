const mockEnv = () => {
  jest.mock('src/lib/env', () => ({
    getEnv: () => {
      return 'development';
    },
    getClientName: () => {
      return 'development';
    },
    getSandcastleURL: () => {
      return 'sandcastleurl';
    },
    isDev: () => {
      return true;
    },
  }));
};

const selectors = require('src/modules/selectors');
const mockSelector = (selector: string, returnValue: any) => {
  const spy = jest.spyOn(selectors, selector);
  spy.mockReturnValue(returnValue);
  return spy;
};

export { mockEnv, mockSelector };
