import React from 'react';
import { Button, Search, SearchIcon } from 'elmo-elements';
import { SCREEN_WIDTH_MOBILE } from 'src/lib/const';
import { isMobileMode } from 'src/lib/util';
import { HeaderSearchWrapper } from 'src/element/HeaderV2/style';
import './style.scss';

export type PropsType = {
  onChange: (newValue: string) => void;
  onSubmit: () => void;
  onClose: () => void;
  className?: string;
  searchValue?: string;
  placeholder?: string;
};

const HeaderSearch = (props: PropsType) => {
  const {
    searchValue,
    onChange,
    onSubmit,
    onClose,
    className,
    placeholder,
  } = props;

  const [screenWidth, setScreenWidth] = React.useState(window.innerWidth);
  const [isSearchOpen, toggleSearchOpen] = React.useState(!isMobileMode());

  function resizeEventHandler() {
    setScreenWidth(window.innerWidth);
    if (isMobileMode()) {
      toggleSearchOpen(false);
    } else {
      toggleSearchOpen(true);
    }
  }

  React.useEffect(() => {
    window.addEventListener('resize', resizeEventHandler);

    return () => window.removeEventListener('resize', resizeEventHandler);
  }, [screenWidth]);

  const openSearch = () => toggleSearchOpen(true);

  const handleOnClose = () => {
    toggleSearchOpen(false);
    onClose();
  };

  return (
    <HeaderSearchWrapper className={className}>
      {screenWidth < SCREEN_WIDTH_MOBILE && (
        <Button
          className="header-v2-search-button"
          onClick={openSearch}
          icon={<SearchIcon />}
        />
      )}
      <Search
        isVisible={isSearchOpen}
        onChange={onChange}
        onSubmit={onSubmit}
        onClose={isMobileMode() ? handleOnClose : undefined}
        value={searchValue}
        placeholder={placeholder}
      />
    </HeaderSearchWrapper>
  );
};

export default HeaderSearch;
