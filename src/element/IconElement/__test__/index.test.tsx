import React from 'react';
import { mount } from 'enzyme';
import IconElement from 'src/element/IconElement';
import ActivityFailedIcon from 'src/element/IconElement/component/ActivityFailedIcon';
import CompletedIcon from 'src/element/IconElement/component/CompletedIcon';
import InProgressIcon from 'src/element/IconElement/component/InProgressIcon';
import NotStartedIcon from 'src/element/IconElement/component/NotStartedIcon';
import RecompletionRequiredIcon from 'src/element/IconElement/component/RecompletionRequiredIcon';

const mountComponent = (Icon: React.ReactNode) => {
  const props = {
    size: 24,
  };

  const wrapper = mount(<IconElement {...props}>{Icon}</IconElement>);

  return { props, wrapper };
};

describe('IconElement', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent(<ActivityFailedIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });

  it('should render activity failed icon', () => {
    const { wrapper } = mountComponent(<ActivityFailedIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(ActivityFailedIcon)).toHaveLength(1);
  });

  it('should render completed icon', () => {
    const { wrapper } = mountComponent(<CompletedIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(CompletedIcon)).toHaveLength(1);
  });

  it('should render in progress icon', () => {
    const { wrapper } = mountComponent(<InProgressIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(InProgressIcon)).toHaveLength(1);
  });

  it('should render not started icon', () => {
    const { wrapper } = mountComponent(<NotStartedIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(NotStartedIcon)).toHaveLength(1);
  });

  it('should render recompletion required icon', () => {
    const { wrapper } = mountComponent(<RecompletionRequiredIcon />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find(RecompletionRequiredIcon)).toHaveLength(1);
  });
});
