import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import httpClient from 'src/lib/api';
import { LANGUAGES } from './messages';
import { TRANSLATIONS_KEY, TRANSLATIONS_EXPIRY_TIME } from 'src/lib/const';
import { getLocalstorage, setLocalstorage } from 'src/lib/util';
import translationMapper from './map';
import logger from 'src/lib/logger';

export { messages, LANGUAGES } from './messages';

/**
 * Fetches translation for client and language
 */
export function fetchTranslations(language: string) {
  // fetching different language logic will be here
  return httpClient.get('/translations');
}

/**
 * Loads translations into localStorage
 * @param language
 */
export async function loadTranslations(
  language: string = LANGUAGES.en
): Promise<any> {
  const storedTranslations = getLocalstorage(TRANSLATIONS_KEY, 'object');
  const currentTime = Date.now();
  let translations = null;
  try {
    if (
      storedTranslations &&
      storedTranslations.timestamp &&
      currentTime - storedTranslations.timestamp < TRANSLATIONS_EXPIRY_TIME
    ) {
      translations = storedTranslations.data;
    }
  } catch (error) {
    return Promise.reject(error);
  }

  // store the base translations in the local storage if they haven't already been
  if (!translations) {
    const fetchedTranslations = await fetchTranslations(language);
    translations = translationMapper(fetchedTranslations.data);

    const localstorageTranslationsData = {
      timestamp: currentTime,
      data: translations,
    };

    setLocalstorage(TRANSLATIONS_KEY, localstorageTranslationsData, 'object');
  }

  i18n.addResourceBundle(
    LANGUAGES.en,
    TRANSLATIONS_KEY,
    translations,
    true,
    true
  );

  return Promise.resolve();
}

/**
 * Wrapper function around the i18next translate function
 * Translates using the default value if translations have not been loaded yet
 *
 * @param translationData
 * @param options
 */
export function t(
  translationData: { key: string; default: string },
  options?: any
) {
  if (translationData === undefined) {
    logger.error('Translation data is undefined');
    return '';
  }

  const { key, default: defaultValue } = translationData;

  if (key === undefined) {
    return '';
  }

  return i18n.t(key, {
    interpolation: { escapeValue: false },
    ...options,
    defaultValue,
  });
}

i18n.use(initReactI18next).init({
  fallbackLng: LANGUAGES.en,
  defaultNS: TRANSLATIONS_KEY,
  // debug: true,
  lng: LANGUAGES.en, // set default language to "en"
});

export default i18n;
