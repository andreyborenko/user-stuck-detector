import React from 'react';
import PageLoading from 'src/element/PageLoading';

interface AppLoadingProps {
  isLoaded: boolean;
  children: JSX.Element;
  error?: Error | null;
}

// This is the loading of the entire APP.
//
// Dev note: Keep this as dumb as possible. In this component we should
// only be concerned about what we should display when the app is in isLoading
// or isLoaded status also what we should display if we have an error
function AppLoading({ children, isLoaded, error }: AppLoadingProps) {
  if (error) {
    return <div>Oh no!!!</div>;
  }

  if (isLoaded) {
    return children;
  }

  return <PageLoading />;
}

export default AppLoading;
