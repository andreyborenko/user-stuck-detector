import React from 'react';
import { map } from 'lodash';
import { Heading, Paragraph, Text, Badge, Button } from 'elmo-elements';
import { getClass } from 'src/lib/util';
import { CardProps } from './type';
import './Card.scss';

/**
 * copied from elmo-elements(v2.0.64) so Learning can do some experiments
 * once, it's confirmed using globally by design team
 * this should go to elmo-elements
 */
const Card = ({
  id,
  heading,
  children,
  description,
  className,
  isFullHeight,
  option,
  prefix,
  image,
  imageAlt,
  badges,
  onClickCard,
  role,
  headerButtons,
}: CardProps) => {
  const renderBadges = () => {
    const keyTemplate = `card-badge-${id}-`;
    return (
      badges &&
      badges.map((badge, index) => (
        <Badge
          icon={badge.icon}
          type={badge.type}
          role={badge.role}
          className={badge.className}
          key={`keyTemplate${index}`}
          data-test-id={keyTemplate + index}
        >
          {badge.children}
        </Badge>
      ))
    );
  };

  const handleClick = () => {
    if (onClickCard) {
      onClickCard();
    }
    return false;
  };

  const handleKeyPress = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      handleClick();
    }
  };

  const backgroundImage = image
    ? { backgroundImage: `url(${image})` }
    : undefined;

  return (
    <>
      {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */}
      <div
        role={role}
        id={id}
        data-testid={`elmo-card-${id || 'default'}`}
        className={getClass('elmo-card', className, {
          'is-full-height': isFullHeight,
          'is-clickable': onClickCard,
        })}
        onClick={onClickCard ? handleClick : undefined}
        onKeyPress={handleKeyPress}
        tabIndex={onClickCard ? 0 : undefined}
      >
        {(image || badges) && (
          <div className="elmo-card__header-image">
            <div
              className="elmo-card__header-image-bg"
              style={{ ...backgroundImage }}
            />
            <div className="elmo-card__header-image-overlay" />
            <img
              className="elmo-card__header-image-img"
              title={image && imageAlt}
              src={image}
              alt={image && imageAlt}
              aria-label={image && imageAlt}
            />
            <div className="elmo-card__header-buttons">
              {map(headerButtons, ({ onClick, ...rest }, index) => (
                <Button
                  key={index}
                  isCircular={true}
                  onClick={(e) => {
                    e.stopPropagation();
                    if (onClick) {
                      onClick(e);
                    }
                  }}
                  {...rest}
                />
              ))}
            </div>
            <div className="elmo-card__header-badges">{renderBadges()}</div>
          </div>
        )}

        {(prefix || heading || description || option) && (
          <div className="elmo-card__header">
            {option && <div className="elmo-card__option">{option}</div>}
            {heading && (
              <Heading className="elmo-card__heading" size="sm">
                {prefix && (
                  <Text
                    size="sm"
                    color="gray"
                    htmlTag="small"
                    className="d-block mb-2 elmo-card__heading-prefix"
                  >
                    {prefix}
                  </Text>
                )}
                {heading}
              </Heading>
            )}
            {description && <Paragraph>{description}</Paragraph>}
          </div>
        )}
        <div className="elmo-card__content">
          <div className="elmo-card__content-inner">{children}</div>
        </div>
      </div>
    </>
  );
};

export default Card;
