import { AppStateType } from './type';

/**
 * Document about what this state is about
 */
export default {
  isLoaded: false,
  isLoading: true,
  config: {},
  error: null,
} as AppStateType;
