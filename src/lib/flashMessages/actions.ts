import { createAction } from 'redux-act';
import shortid from 'shortid';
import { Message } from './types';

export const addFlashMessage: any = createAction(
  'PMS_ADD_FLASH_MESSAGE',
  (payload: Message) => {
    // Generate a ID if it does not exist in payload
    if (!payload.id) {
      payload.id = shortid.generate();
    }
    return { ...payload };
  }
);

export const showFlashMessage: any = createAction(
  'PMS_SHOW_FLASH_MESSAGE',
  (payload: Message) => payload
);

export const removeFlashMessage: any = createAction(
  'PMS_REMOVE_FLASH_MESSAGE',
  (payload: Message) => payload
);

export const dismissFlashMessage: any = createAction(
  'PMS_DISMISS_FLASH_MESSAGE',
  (payload: Message) => payload
);
