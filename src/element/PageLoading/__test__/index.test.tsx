import React from 'react';
import { mount } from 'enzyme';
import PageLoading from 'src/element/PageLoading';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<PageLoading {...props} />);

  return { props, wrapper };
};

describe('PageLoading', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
});
