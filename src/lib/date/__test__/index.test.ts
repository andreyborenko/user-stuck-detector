import { dueDateFormat, unixDate, DATE_FORMAT } from 'src/lib/date';
import moment from 'moment';
import { messages, t } from 'src/lib/translation';

describe('dueDateFormat', () => {
  test('should return false if data is not passed to dueDateFormat', () => {
    expect(dueDateFormat()).toBeFalsy;
  });

  test('data should be formatted', () => {
    const mockUnixTimestamp = 1872217346;
    const mockFormattedTimestamp = unixDate(mockUnixTimestamp).format(
      DATE_FORMAT
    );
    const result = dueDateFormat(mockUnixTimestamp);

    expect(result).toEqual(mockFormattedTimestamp);
  });

  test('if due date is today should return `Today` ', () => {
    const date = moment().unix();
    const result = dueDateFormat(date);

    expect(result).toEqual(t(messages.myLearning.date.today));
  });

  test('if due date is in 1 day should return `in 1 day` ', () => {
    const date = moment().add(1, 'day').endOf('day').unix();
    const result = dueDateFormat(date);

    expect(result).toEqual(t(messages.myLearning.date.day));
  });

  test('if due date is less then 14 should return string with number of days left', () => {
    const date = moment().add(10, 'days').endOf('day').unix();
    const result = dueDateFormat(date);

    expect(result).toEqual(t(messages.myLearning.date.days, { diff: 10 }));
  });
});
