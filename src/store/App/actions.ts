import { createAction } from 'redux-act';

/**
 * Some short document about what this action is about
 * <Dev.Note>
 *  Action names should follow the following convention. Note that the sub section is optional
 *  [app abbreviation]_[section name]_[sub section name?]_[action]_[null|SUCCESS|FAILURE]
 * </Dev.Note>
 */
export const appInit = createAction('PMS_APP_INIT');
export const appInitSuccess = createAction(
  'PMS_APP_INIT_SUCCESS',
  (data) => data
);
export const appInitFailure = createAction<Error | null>(
  'PMS_APP_INIT_FAILURE'
);
export const appClear = createAction('APP_CLEAR');
