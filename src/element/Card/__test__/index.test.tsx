import React from 'react';
import { mount } from 'enzyme';
import { Heading, Paragraph, Text, Badge } from 'elmo-elements';
import Card from 'src/element/Card';
import { CardProps } from 'src/element/Card/type';

const defaultProps: CardProps = undefined;
const mountComponent = (customProps: any = {}) => {
  const props = { ...defaultProps, ...customProps };
  const wrapper = mount(<Card {...props} />);
  const component = wrapper.find('.elmo-card');
  const instance = wrapper.instance();
  return { props, wrapper, component, instance };
};

describe('Card', () => {
  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
    });

    it.each([
      ['Heading exist', { heading: 'Title' }],
      ['Description exist', { description: 'Description' }],
    ])('should be match with snapshot %s', (name, props) => {
      const { wrapper } = mountComponent(props);
      expect(wrapper).toMatchSnapshot();
    });

    it.each([
      ['role', 'test'],
      ['role', undefined],
    ])(
      'should be match with snapshot when props %s is %s',
      (propsName, propsValue) => {
        const { wrapper } = mountComponent({ [propsName]: propsValue });
        expect(wrapper).toMatchSnapshot();
      }
    );
  });

  describe('props', () => {
    describe('default', () => {
      it('should have expected default props', () => {
        expect(JSON.stringify(defaultProps)).toBe(
          JSON.stringify(Card.defaultProps)
        );
      });

      it.each([
        ['elmo-card-default', {}],
        ['elmo-card-custom-id', { id: 'custom-id' }],
      ])('should render test ID properly', (expectedTestId, props) => {
        const { component } = mountComponent(props);
        expect(component.prop('data-testid')).toBe(expectedTestId);
      });
    });

    describe('children', () => {
      it('should render children', () => {
        const { wrapper } = mountComponent({ children: 'Hello world' });
        expect(wrapper.find('.elmo-card__content').text()).toBe('Hello world');
      });
    });

    describe('custom', () => {
      it.each([['id', 'test', { id: 'test' }]])(
        'should render prop %s as %s',
        (name, expected, props) => {
          const { component } = mountComponent(props);
          expect(component.prop(name)).toBe(expected);
        }
      );

      it.each([['role', 'test', { role: 'test' }]])(
        'should render prop %s as %s',
        (name, expected, props) => {
          const { component } = mountComponent(props);
          expect(component.prop(name)).toBe(expected);
        }
      );
    });
  });

  describe('interaction', () => {
    it('should add alt/title in image', () => {
      const props = {
        image: 'some image url',
        imageAlt: 'some text',
      };
      const { wrapper } = mountComponent(props);

      expect(wrapper.find('.elmo-card__header-image-img').prop('title')).toBe(
        props.imageAlt
      );
      expect(
        wrapper.find('.elmo-card__header-image-img').prop('aria-label')
      ).toBe(props.imageAlt);
    });

    it.each([
      ['should', '.elmo-card', 1, {}, '.elmo-card'],
      [
        'should',
        '.custom-class',
        2,
        { className: 'custom-class' },
        '.custom-class',
      ],
      [
        'should',
        '.elmo-card--is-full-height',
        1,
        { isFullHeight: true },
        '.elmo-card--is-full-height',
      ],
      [
        'should not',
        '.elmo-card--is-full-height',
        0,
        { isFullHeight: false },
        '.elmo-card--is-full-height',
      ],
      [
        'should',
        '.elmo-card__option',
        1,
        { option: 'some option' },
        '.elmo-card__option',
      ],
      ['should not', '.elmo-card__option', 0, {}, '.elmo-card__option'],
      ['should', '.elmo-card__content', 1, {}, '.elmo-card__content'],
      [
        'should',
        '.elmo-card__header',
        1,
        { heading: 'some heading' },
        '.elmo-card__header',
      ],
      [
        'should',
        '.elmo-card__header',
        1,
        { description: 'some description' },
        '.elmo-card__header',
      ],
      ['should', 'Heading', 1, { heading: 'some heading' }, Heading],
      ['should not', 'Heading', 0, {}, Heading],
      [
        'should',
        'Paragraph',
        1,
        { description: 'some description' },
        Paragraph,
      ],
      ['should not', 'Paragraph', 0, {}, Paragraph],
      [
        'should',
        'Text',
        1,
        { heading: 'some heading', prefix: 'some prefix' },
        Text,
      ],
      ['should not', 'Text', 0, {}, Text],
      [
        'should',
        '.elmo-card__header-image',
        1,
        { image: 'some image url' },
        '.elmo-card__header-image',
      ],
      [
        'should not',
        '.elmo-card__header-image',
        0,
        {},
        '.elmo-card__header-image',
      ],
      [
        'should',
        'Badge',
        1,
        {
          badges: [{ children: 'some children text' }],
        },
        Badge,
      ],
      ['should not', 'Badge', 0, {}, Badge],
    ])('%s have %s', (name, element, expected, props, findElement) => {
      const { wrapper } = mountComponent(props);
      expect(wrapper.find(findElement)).toHaveLength(expected);
    });

    describe('Interaction', () => {
      it('handleClick should call onClickCard', () => {
        const props = {
          onClickCard: jest.fn(),
        };
        const { wrapper } = mountComponent(props);
        wrapper.simulate('click');
        expect(props.onClickCard).toBeCalled();
      });
      it('handleKeyPress Enter should call onClickCard', () => {
        const props = {
          onClickCard: jest.fn(),
        };
        const { wrapper } = mountComponent(props);
        wrapper.simulate('keypress', { key: 'Enter' });
        expect(props.onClickCard).toBeCalled();
      });
    });
  });
});
