import React from 'react';
import { shallow } from 'enzyme';
import AppLayout from 'src/layout/AppLayout';

const shallowComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = shallow(<AppLayout {...props} />);

  return { props, wrapper };
};

describe('AppLayout', () => {
  it('should render component without any crash', () => {
    const { wrapper } = shallowComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
});
