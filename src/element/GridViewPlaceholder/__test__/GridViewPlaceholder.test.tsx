import React from 'react';
import { mount } from 'enzyme';
import GridViewPlaceholder from 'src/element/GridViewPlaceholder/';
import { GridViewPlaceholderPropsType } from 'src/element/GridViewPlaceholder/type';

const mockMath = Object.create(global.Math);
mockMath.random = () => 0;
global.Math = mockMath;

const defaultProps: GridViewPlaceholderPropsType = {
  cols: 12,
  rows: 1,
};

const mountComponent = (customProps: any = {}) => {
  const props = { ...defaultProps, ...customProps };
  const wrapper = mount(<GridViewPlaceholder {...props} />);
  const component = wrapper.find('.grid-view-placeholder');

  return { props, wrapper, component };
};

describe('GridViewPlaceholder', () => {
  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
      expect(wrapper.find('.elmo-row')).toHaveLength(defaultProps.rows);
      expect(wrapper.find('.elmo-col')).toHaveLength(
        defaultProps.rows * defaultProps.cols
      );
    });

    it('should be match with snapshot', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toMatchSnapshot(1);
    });
  });
});
