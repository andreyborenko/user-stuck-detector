import React from 'react';
import { Notification } from 'elmo-elements';
import { isFunction } from 'lodash';
import { Message } from './types';

export interface FlashMessagesViewProps {
  message: Message;
  handleClose?: (message: Message) => void;
}

const FlashMessagesView = ({
  message,
  handleClose,
}: FlashMessagesViewProps) => {
  try {
    const onClose = () => {
      if (handleClose) {
        handleClose(message);
      }
    };
    const onUndo = () => {
      message.onUndo();
      onClose();
    };
    return (
      <div className={message.className}>
        <Notification
          isActive={message.isActive}
          message={message.message}
          undoBtnText={message.undoBtnText}
          onUndo={isFunction(message.onUndo) ? onUndo : undefined}
          onClose={onClose}
        />
      </div>
    );
  } catch (error) {
    return null;
  }
};

export default FlashMessagesView;
