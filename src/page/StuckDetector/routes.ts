import StuckDetector from '.';
import { setBasePath } from 'src/lib/route';

const StuckDetectorDashboardRoutes: any = {
  path: setBasePath('/dashboard'),
  component: StuckDetector,
  title: 'Stuck Detector',
};

export default StuckDetectorDashboardRoutes;
