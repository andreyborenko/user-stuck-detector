import { SagaIterator } from 'redux-saga';
import { call, put, takeEvery } from 'redux-saga/effects';
import { appInit, appInitSuccess, appInitFailure } from './actions';
import setup from 'src/lib/setup';

/**
 * Get appraisal
 */
export const appInitSaga = function* (): SagaIterator {
  try {
    // <Dev.Note>
    //  If the async activities you need to do is big or complex it is better to abstract your API
    //  in a separate service for example in this case 'lib/setup'. The advantage of this abstraction
    //  is to have 1) better test ability and control on your API 2) keep the size of
    //  this saga small
    // </Dev.Note>
    const data = yield call(setup);
    yield put(appInitSuccess(data));
  } catch (error) {
    yield put(appInitFailure(error));
  }
};

/**
 * Apprisal Sagas
 */
export default function* root() {
  yield takeEvery(appInit, appInitSaga);
}
