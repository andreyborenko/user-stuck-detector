import React from 'react';
import { mount } from 'enzyme';
import NotFound from 'src/element/NotFound';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<NotFound {...props} />);

  return { props, wrapper };
};

const selectors = {
  image: 'img',
  header: '[data-testid="elmo-message-block-header-default"]',
  body: '[data-testid="elmo-message-block-body-default"]',
};

describe('NotFound', () => {
  describe('rendering', () => {
    it('should render component without any crash', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toHaveLength(1);
    });

    it('should be match with snapshot', () => {
      const { wrapper } = mountComponent();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('props', () => {
    describe('default', () => {
      it('should not render image', () => {
        const { wrapper } = mountComponent();
        expect(wrapper.find(selectors.image)).toHaveLength(0);
      });

      it('header content should be empty', () => {
        const { wrapper } = mountComponent();
        expect(wrapper.find(selectors.header).text()).toEqual('');
      });

      it('body content should be empty', () => {
        const { wrapper } = mountComponent();
        expect(wrapper.find(selectors.body).text()).toEqual('');
      });

      it('should not render a button', () => {
        const { wrapper } = mountComponent();
        expect(wrapper.find(selectors.body).find('button')).toHaveLength(0);
      });
    });

    describe('custom', () => {
      const customProps = {
        imageUrl: 'images/image.png',
        title: 'not found title',
        paragraph: 'not found paragraph',
        button: <button>text</button>,
      };

      it('should render image', () => {
        const { wrapper } = mountComponent(customProps);
        expect(wrapper.find('img').prop('src')).toEqual(customProps.imageUrl);
      });

      it('should render header content', () => {
        const { wrapper } = mountComponent(customProps);
        expect(wrapper.find(selectors.header).text()).toContain(
          customProps.title
        );
      });

      it('should render body content', () => {
        const { wrapper } = mountComponent(customProps);
        expect(wrapper.find(selectors.body).text()).toContain(
          customProps.paragraph
        );
      });

      it('should render button', () => {
        const { wrapper } = mountComponent(customProps);
        expect(wrapper.find('button')).toHaveLength(1);
      });
    });
  });
});
