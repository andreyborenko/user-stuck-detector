import i18n from 'i18next';
import { HttpClient } from 'elmo-react-core';
import httpClient from 'src/lib/api';
import { fetchTranslations, loadTranslations, t } from 'src/lib/translation';
import { setLocalstorage } from 'src/lib/util';
import { LANGUAGES } from 'src/lib/translation/messages';

import { mockEnv } from 'src/lib/test';

mockEnv();

jest.mock('src/lib/translation/map', () => {
  return (data: any) => {
    return data;
  };
});

let translationData = {};
jest.mock('i18next', () => ({
  addResourceBundle: (
    lang: any,
    key: any,
    data: any,
    paramOne: boolean,
    paramTwo: boolean
  ) => {
    translationData = data;
  },
  use: () => {
    return { init: () => undefined };
  },
  t: (key: any, options: any) => undefined,
}));
jest.mock('elmo-react-core');

jest.mock('src/lib/logger', () => {
  return false;
});

const mockTranslationData = {
  someTranslationKey: 'someTranslationValue',
};
let mockCurrentTimestamp = Date.now();
jest.mock('src/lib/util', () => ({
  getLocalstorage: (key: any, type: any) => {
    return {
      timestamp: mockCurrentTimestamp,
      data: mockTranslationData,
    };
  },
  setLocalstorage: jest.fn(),
}));

describe('lib/translation', () => {
  describe('t', () => {
    it('should return empty string if key is not provided', () => {
      const spy = jest.spyOn(i18n, 't');
      const someObject = {
        default: 'someValue',
      };
      const tResult = t(someObject);
      expect(tResult).toBe('');
    });

    it('should call i18next translate function', () => {
      const spy = jest.spyOn(i18n, 't');
      const someObject = {
        key: 'someKey',
        default: 'someValue',
      };
      t(someObject);
      expect(spy).toBeCalledWith(someObject.key, {
        defaultValue: someObject.default,
        interpolation: { escapeValue: false },
      });
    });
  });

  describe('fetchTranslations', () => {
    it('should call HttpClient', () => {
      fetchTranslations(LANGUAGES.en);
      expect(HttpClient).toBeCalledTimes(1);
    });
  });

  describe('loadTranslations', () => {
    it('should call HttpClient', () => {
      loadTranslations(LANGUAGES.en);
      expect(HttpClient).toBeCalledTimes(1);
    });
    it('should load translation from localstorage', () => {
      loadTranslations(LANGUAGES.en);
      expect(translationData).toBe(mockTranslationData);
    });
    it('should fetch translation from api if there is no in localstorage or it is expired', () => {
      mockCurrentTimestamp = 0;
      const mockTranslationDataTwo = {
        someTranslationKey: 'someTranslationValueTwo',
      };
      httpClient.get.mockResolvedValue({ data: mockTranslationDataTwo });
      loadTranslations(LANGUAGES.en).then(() => {
        expect(translationData).toBe(mockTranslationDataTwo);
        expect(setLocalstorage).toBeCalledTimes(1);
      });
    });
  });
});
