import { get, forIn, set } from 'lodash';

function translationMapper(fetchedTranslations: any) {
  // key : BE translations key
  // value : FE translations key[]
  // One translation can be placed in multiple places.
  const translationList = {
    'cpd.word.tabName': ['professionalDevelopment.title'],
    'cpd.word.fieldName': [
      'courseCatalogue.columnLabel.cpd',
      'manageExternalTraining.cpd.points.label',
      'myExternalTraining.columnLabel.cpdPoints',
      'myExternalTraining.sortableColumns.cpdPoints',
      'courseCatalogue.card.cpdPoints',
      'courseCatalogue.sort.cpdPoints',
      'viewCourse.courseInfo.cpd',
      'viewMyExternalTraining.trainingInfo.cpd',
    ],
  };

  const mappedTranslations = {};

  forIn(translationList, (targetKeys: string[], key: string) => {
    const translationProvided = get(fetchedTranslations, key);
    if (translationProvided) {
      targetKeys.forEach((targetKey) => {
        set(mappedTranslations, targetKey, translationProvided);
      });
    }
  });
  return mappedTranslations;
}

export default translationMapper;
