import React from 'react';
import { mount, shallow } from 'enzyme';
import withFeatureFlagHoc from '../withFeatureFlagHoc';
import { Provider } from 'react-redux';
import store from 'src/store/store';

const mockProps = {
  someProp: 'some value',
};

const TestComponent = () => {
  return <span>Test Component</span>;
};

const setup = (props: any) => {
  const ComponentWithHoc = withFeatureFlagHoc(<TestComponent />);
  const wrapper = shallow(
    <Provider store={store}>
      <ComponentWithHoc {...props} />
    </Provider>
  );

  return { wrapper };
};

describe('withFeatureFlagHoc', () => {
  it('should render self and subcomponent', () => {
    const { wrapper } = setup(mockProps);
    expect(wrapper.children()).toHaveLength(1);
    expect(wrapper.children().prop('someProp')).toBe('some value');
  });
});
