import React, { useState } from 'react';
import { filter, find } from 'lodash';
import { useDispatch } from 'react-redux';
import { HeaderTabs, HeaderV2, Layout } from 'src/element';
import { addFlashMessage } from 'src/lib/flashMessages/actions';
import { Users, Summary } from './components';
import { users } from './const';
import {
  addToEventLog,
  findUsers,
  getUsersFromLS,
  mapUsersToDropdownOptions,
  setUsersToLS,
  logActionsWithCriteria,
} from './helpers';
import { UserActionTypes } from './type';

logActionsWithCriteria();

const headerTabs = [
  { label: 'Users', value: 'users', component: Users },
  { label: 'Summary', value: 'summary', component: Summary },
];

export const StuckDetectorView = () => {
  const dispatch = useDispatch();
  const initialUsers = mapUsersToDropdownOptions(getUsersFromLS());
  const [selectedTab, setSelectedTab] = useState(headerTabs[0].value);
  const [selectedUsers, setSelectedUsers] = useState(initialUsers);
  const [changedUsers, setChangedUsers] = useState<any[]>([]);

  const handleTabSelect = (value: any) => {
    addToEventLog({ type: 'select_tab', label: value });
    setSelectedTab(value);
  };

  const handleSelectUser = (
    action: UserActionTypes,
    modifiedOption: any,
    newUserOptions: any[]
  ) => {
    addToEventLog({ type: action, label: `${action}-${modifiedOption.value}` });
    setSelectedUsers(newUserOptions);
  };

  const onSave = () => {
    const removedUsers = findUsers(initialUsers, selectedUsers, 'remove-value');
    const addedUsers = findUsers(selectedUsers, initialUsers, 'select-option');
    const newUsers = [...removedUsers, ...addedUsers];
    setChangedUsers(newUsers);

    addToEventLog({ type: 'button', label: 'Save' });
    dispatch(
      addFlashMessage({
        isActive: true,
        message: newUsers.length
          ? 'User changes saved'
          : 'There is no change found',
      })
    );
  };

  const onCancel = () => {
    addToEventLog({ type: 'button', label: 'Cancel' });
    setSelectedUsers(initialUsers);
  };

  const onConfirm = () => {
    addToEventLog({ type: 'button', label: 'Confirm' });
    setUsersToLS(selectedUsers);
    setChangedUsers([]);
    dispatch(
      addFlashMessage({
        isActive: true,
        message: 'Changes are succesfully saved',
      })
    );
  };

  const onUndo = (id: string, action: UserActionTypes) => {
    addToEventLog({ type: 'button', label: `undo-${id}` });
    setChangedUsers(filter(changedUsers, (user) => user.id !== id));

    if (action === 'remove-value') {
      const foundUser = find(initialUsers, { id });
      setSelectedUsers([...selectedUsers, foundUser]);
    }

    if (action === 'select-option') {
      setSelectedUsers(filter(selectedUsers, (user) => user.id !== id));
    }
  };

  return (
    <>
      <Layout.Header>
        <HeaderV2
          className={'header-v2-layout'}
          title={'Stuck Detector'}
          tabs={
            <HeaderTabs
              items={headerTabs}
              handleChange={handleTabSelect}
              selected={selectedTab}
            />
          }
        />
      </Layout.Header>

      <Layout.Content>
        {selectedTab === 'users' && (
          <Users
            users={users}
            selectedUsers={selectedUsers}
            onSelectUsers={handleSelectUser}
            onSave={onSave}
            onCancel={onCancel}
          />
        )}
        {selectedTab === 'summary' && (
          <Summary
            changedUsers={changedUsers}
            onUndo={onUndo}
            onConfirm={onConfirm}
          />
        )}
      </Layout.Content>
    </>
  );
};

export default StuckDetectorView;
