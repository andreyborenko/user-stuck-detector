import React from 'react';
import { mount } from 'enzyme';
import NotFound from 'src/page/NotFound';

const mountComponent = (customProps: any = {}) => {
  const props = { ...customProps };
  const wrapper = mount(<NotFound {...props} />);

  return { props, wrapper };
};

describe('NotFoundContainer', () => {
  it('should render component without any crash', () => {
    const { wrapper } = mountComponent();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper).toHaveLength(1);
  });
});
