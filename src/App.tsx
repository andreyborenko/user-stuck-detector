import React from 'react';
import { connect } from 'react-redux';
import { Router } from 'react-router-dom';
import {
  defaultTheme,
  ApplicationTheme,
  ThemeProvider as AppThemeProvider,
} from 'elmo-elements';
import styled, { ThemeProvider } from 'styled-components';
import { get } from 'lodash';
import { getTheme } from './lib/theme';
import history from './lib/history';
import AppLayout, { AppLoading } from './layout';
import FlashMessages from './lib/flashMessages';
import { appInit } from './store/App';
import { ReduxStoreType } from './store/type';
import { AppStateType } from './store/App/type';

interface AppProps {
  appInit: any;
  app: AppStateType;
}

type AppStates = {
  theme: typeof defaultTheme;
};

const StyledLayout = styled.div<{ theme?: typeof defaultTheme }>`
  .elmo-elements{
    font-family: ${({ theme }) => get(theme, 'font')} !important;
  }
  .elmo-layout__main-app {
    a,
    a:hover {
      color: ${({ theme }) => theme.link.color};
    },
  }
  .elmo-nav-submenu__items {
    background-color: #19579f !important;
    color: #ffffff !important;
    a {
      color: #ffffff !important;
    }
    a:hover {
      color: #ffffff !important;
    }
    li.is-selected a {
      color: #ffffff !important;
      font-weight: 600;
    }
    li.is-selected a:hover {
      font-weight: 600;
      color: #ffffff !important;
    }
    li.is-selected .elmo-icon:before {
      content: "";
      width: 32px;
      height: 32px;
      border-radius: 50%;
      display: block;
      position: absolute;
      left: 50%;
      top: 50%;
      -webkit-transform: translateX(-50%) translatey(-50%);
      transform: translateX(-50%) translatey(-50%);
      background-color: hsla(0,0%,100%,.15);
    }
  }
  .elmo-nav-submenu__header {
    background-color:  ${({ theme }) =>
      get(theme, 'menu.headingBannerBgColor')};
    color: ${({ theme }) =>
      get(theme, 'menu.headingBannerUserMenuColor')} !important;
    font-size: 16px;
    letter-spacing: 1px;
    a {
      color: ${({ theme }) =>
        get(theme, 'menu.headingBannerUserMenuColor')} !important;
    }
  }
`;

export class App extends React.Component<AppProps, AppStates> {
  state: AppStates = {
    theme: defaultTheme,
  };

  componentDidMount() {
    this.props.appInit();
    this.getTheme();
  }

  async getTheme() {
    // this.setState({ theme: await getTheme() });
  }

  render() {
    const {
      app: { isLoaded, error },
    } = this.props;
    const { theme } = this.state;

    return (
      <AppLoading isLoaded={isLoaded} error={error}>
        <AppThemeProvider theme={theme}>
          <ThemeProvider theme={theme}>
            <>
              <Router history={history}>
                <StyledLayout theme={theme}>
                  <AppLayout />
                </StyledLayout>
                <FlashMessages />
              </Router>
              <ApplicationTheme />
            </>
          </ThemeProvider>
        </AppThemeProvider>
      </AppLoading>
    );
  }
}

export const mapStateToProps = (state: ReduxStoreType) => ({ app: state.app });
export const mapDispatchToProps = { appInit };

export default connect(mapStateToProps, mapDispatchToProps)(App);
