const translations = {
  app: {
    notfound: {
      title: 'Localhost',
    },
  },
  cpd: { word: { fieldName: 'CPD Hours', tabName: 'CPD' } },
};
module.exports = translations;
