import React, { ReactNode } from 'react';
import { MessageBlock, Paragraph } from 'elmo-elements';

export type PropsType = {
  imageUrl?: string;
  title?: string | ReactNode;
  paragraph?: string | ReactNode;
  button?: ReactNode;
};

export default function NotFound({
  imageUrl = '',
  title = '',
  paragraph = '',
  button = '',
}: PropsType) {
  return (
    <>
      {imageUrl ? (
        <Paragraph className="text-center mb-4 mt-5">
          <img src={imageUrl} alt="Not found" />
        </Paragraph>
      ) : (
        <>
          <br />
          <br />
        </>
      )}

      <MessageBlock>
        <MessageBlock.Header>{title}</MessageBlock.Header>
        <MessageBlock.Body>{paragraph}</MessageBlock.Body>
        {button}
      </MessageBlock>
    </>
  );
}
