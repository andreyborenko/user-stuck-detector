import React from 'react';
import withApiRequest from 'src/element/withApiRequest/index';
import { mount } from 'enzyme';

const api = require('src/modules/utils/api');

jest.mock('src/modules/utils/api');

const TestComponent = () => <span>Test with api request</span>;

const setup = (props?: any) => {
  const ComponentWithHoc = withApiRequest(TestComponent, {
    request: 'CourseEnrolments.getAll',
    params: { someParam: 'some value' },
  });

  const wrapper = mount(<ComponentWithHoc {...props} />);

  return { wrapper };
};

describe('withApiRequest', () => {
  it('should render wrapped component', () => {
    const { wrapper } = setup();

    expect(wrapper).toHaveLength(1);
    expect(wrapper.find('span').text()).toBe('Test with api request');
  });

  it('should pass props to hoc', (done) => {
    const data = {
      data: [{ id: 1 }],
      isLoading: false,
      isLoaded: true,
      error: null,
    };

    api.Api.CourseEnrolments.getAll = jest.fn(() => Promise.resolve(data));

    const { wrapper } = setup();

    setImmediate(() => {
      // within `setImmediate` all of the promises have been exhausted
      wrapper.update();
      expect(wrapper.children().prop('response')).toStrictEqual(data);
      // have to call `done` here to let Jest know the test is done
      done();
    });
  });
});
