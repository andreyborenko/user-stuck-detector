import React from 'react';
import { RouteProps, matchPath } from 'react-router';
import { pickBy, has, forEach, get } from 'lodash';
import { messages } from 'src/lib/translation';

export { default as NestedRoutes } from './NestedRoutes';
export { default as withRouteAware } from './RouteAware';

export interface DeafultRoute<T extends RouteProps = RouteProps> {
  path: string;
  component: React.ComponentType<any>;
  routes?: RouteType[];
  title?: any;
  routeComponent?: React.ComponentType<T>;
}

export interface RedirectRoute {
  path: string;
  redirect: string;
  exact?: boolean;
}

export type RouteType = DeafultRoute | RedirectRoute;

export function setBasePath(path: string) {
  return `/stuck-detector${path}`;
}

/**
 * Returns current page key of route from pageRoutes
 */

export const getCurrentPageKey = () => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { pageRoutes } = require('src/routes');
  const routePaths = pickBy(pageRoutes, (route, key) => has(route, 'path'));

  let currentPage = '';
  forEach(routePaths, (route: Record<string, any>, key: string) => {
    if (matchPath(window.location.pathname, route) !== null) {
      currentPage = key;
      return false;
    }
    return true;
  });
  return currentPage;
};

/**
 * Returns current page title from translation
 */
export function getCurrentPageTitle() {
  const pageKey = getCurrentPageKey();
  const getTranslationData = get(messages.pages, pageKey);
  return getTranslationData ? getTranslationData.default : '';
}
